using UnityEditor;

namespace LokiPlugin {
    [InitializeOnLoad]
    class Startup {
        static Startup() {
            Loki.OnStartup();
            LokiWindow.OnStartup();
            GizmoDrawer.OnStartup();
        }
    }
}