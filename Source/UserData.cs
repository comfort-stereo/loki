﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;

namespace LokiPlugin {

    class UserData : ScriptableObject {

        [SerializeField] public float RotationDelta = 15f;
        [SerializeField] public GridLockMode GridLockMode; 
        [SerializeField] public bool GridLockEnabled = true;
        [SerializeField] public bool AlwaysShowGrid = true;
        [SerializeField] public bool HideUnityGridWhenGridIsShown = true;
        [SerializeField] public bool CloseWindowWhenPlacingPrefab = true;
        [SerializeField] public EventModifiers KeybindingModifiersRotate = EventModifiers.Shift;
        [SerializeField] public EventModifiers KeybindingModifiersChangeBrushScale = EventModifiers.Shift | EventModifiers.Alt;
        [SerializeField] public LokiKeybinding KeybindingReset = new LokiKeybinding(KeyCode.Escape);
        [SerializeField] public LokiKeybinding KeybindingGrabGameObjects = new LokiKeybinding(KeyCode.A, requiresSceneFocus: true);
        [SerializeField] public LokiKeybinding KeybindingDuplicateAndGrabGameObjects = new LokiKeybinding(KeyCode.Z, requiresSceneFocus: true);
        [SerializeField] public LokiKeybinding KeybindingToggleGridLock = new LokiKeybinding(KeyCode.S, requiresSceneFocus: true);
        [SerializeField] public LokiKeybinding KeybindingToggleWindow = new LokiKeybinding(KeyCode.BackQuote);
        [SerializeField] public LokiKeybinding KeybindingSelectGridSizeUp = new LokiKeybinding(KeyCode.D, requiresSceneFocus: true);
        [SerializeField] public LokiKeybinding KeybindingSelectGridSizeDown = new LokiKeybinding(KeyCode.D, EventModifiers.Shift, requiresSceneFocus: true);
        [SerializeField] public int SelectedGridSizeIndex;
        [SerializeField] public int SelectedPaletteIndex;
        [SerializeField] public List<float> GridSizes = new List<float> { 1f };
        [SerializeField] public List<LokiPalette> Palettes = new List<LokiPalette> { new LokiPalette() };
        [SerializeField] public string BasePrefabDirectory = "Assets";
        [SerializeField] public List<string> IgnoredPrefabDirectories = new List<string> { "Assets/Editor Default Resources" };
        [SerializeField] SerializableColor highlightColor = new SerializableColor(Color.cyan);
        [SerializeField] SerializableColor gridColor = new SerializableColor(new Color(0f, 1f, 1f, 0.01f));
        [SerializeField] SerializableColor gridPlacementOverlapColor = new SerializableColor(new Color(1f, 0.9f, 0.01f, 0.2f));
        
        public Color HighlightColor {
            get { return this.highlightColor.Color; }
            set { this.highlightColor.Set(value); }
        }
        
        public Color GridColor {
            get { return this.gridColor.Color; }
            set { this.gridColor.Set(value); }
        }

        public Color GridPlacementOverlapColor {
            get { return this.gridPlacementOverlapColor.Color; }
            set { this.gridPlacementOverlapColor.Set(value); }
        }

        void OnEnable() {
            this.SelectedGridSizeIndex = this.SelectedGridSizeIndex.InBoundsOf(this.GridSizes);
            this.SelectedPaletteIndex = this.SelectedPaletteIndex.InBoundsOf(this.Palettes);
        }

        void OnDisable() {
            EditorUtility.SetDirty(this);
        }
    }
}
