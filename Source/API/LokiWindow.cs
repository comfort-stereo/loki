﻿using UnityEditor;
using UnityEngine;
using System.Collections.Generic;

namespace LokiPlugin {

    public partial class LokiWindow : EditorWindow {

        Loki loki;

        const int PaletteTabsPerRow = 4;
        const int GridSizesPerRow = 4;
        const int MinimumWidth = 30;

        const float MinimumPaletteEntrySize = 32;
        const float MaximumPaletteEntrySize = 128;
        const float PaletteEntryBorderSize = 14;
        const float PaletteEntryInsetFactor = 0.85f;
        const float PaletteEntryLabelY = -12;
        const float PaletteEntrySymbolDeltaY = 10;
        const float PaletteSearchHeight = 150;
        const float FoldoutWindowWidth = 300;
        const float FoldoutWindowLayoutWidth = FoldoutWindowWidth - 4;

        Color paletteEntryUnselectedColor = Color.gray;

        float enteredGridSize;
        float paletteEntrySize = (MaximumPaletteEntrySize + MinimumPaletteEntrySize) / 2;

        bool addingNewGridSize;
        bool showPlacementOptions;
        bool showAssociatedSearches; 
        bool showInlinePrefabInspector = false;
        bool repainting;
        bool showingOptions;
        bool showingPaletteEditor;

        Vector2 paletteScrollPosition;
        Vector2 addedPrefabsScroll;
        Vector2 paletteSearchesScroll;
        Vector2 smallPaletteEditorSearchesScroll;
        Vector2 keybindingsScroll;
        Vector2? mouseDragPositionStart;

        List<Callback> deferredActions = new List<Callback>();

        /// <summary>
        /// Run from startup initializer.
        /// </summary>
        internal static void OnStartup() {
            GiveControllerToBackend(Loki.Instance);
        }

        /// <summary>
        /// Give the backend a way to control the window without actually knowing the implementation. 
        /// </summary>
        /// <param name="loki">The backend instance to give a controller.</param>
        static void GiveControllerToBackend(Loki loki) {
            loki.WindowController = new WindowController(
                onIsOpen: () => {
                    return IsOpen;
                },
                onIsFocused: () => {
                    return IsOpen && EditorWindow.focusedWindow == GetWindow(false);
                },
                onFocus: () => {
                    if (IsOpen) {
                        GetWindow();
                    }
                },
                onRefresh: () => {
                    if (IsOpen) {
                        GetWindow(focus: false).Repaint();
                    }
                },
                onOpen: Open,
                onClose: Close,
                onToggle: Toggle
            );
        }

        /// <summary>
        /// Defer an action that modifies the state of the window until the end of OnGUI().
        /// </summary>
        /// <param name="action">The action to defer.</param>
        void DeferAction(Callback action) {
            this.deferredActions.Add(action);
        }

        /// <summary>
        /// Remove all deferred actions from the queue. 
        /// </summary>
        void ClearDeferredActions() {
            this.deferredActions.Clear();
        }

        /// <summary>
        /// Specifies if the window is open in the editor.
        /// </summary>
        public static bool IsOpen { get; private set; }

        /// <summary>
        /// Specifies whether Loki's options menu is shown.
        /// </summary>
        public bool ShowOptions {
            get { return this.showingOptions; }
            set {
                if (value == this.showingOptions) return;
                this.showingOptions = value;
                var delta = this.showingOptions ? FoldoutWindowWidth : -FoldoutWindowWidth;
                this.position = this.position.SetWidth(this.position.width + delta);
                Repaint();
            }
        }

        /// <summary>
        /// Specifies whether Loki's palette editor is shown.
        /// </summary>
        public bool ShowPaletteEditor {
            get { return this.showingPaletteEditor; }
            set {
                if (value == this.showingPaletteEditor) return;
                this.showingPaletteEditor = value;
                var delta = this.showingPaletteEditor ? FoldoutWindowWidth : -FoldoutWindowWidth;
                this.position = this.position.SetWidth(this.position.width + delta);
                Repaint();
            }
        }

        /// <summary>
        /// The the total space each palette entry occupies in the palette including the border size.
        /// </summary>
        float TotalPaletteEntrySize {
            get { return this.paletteEntrySize + PaletteEntryBorderSize; }
        }

        /// <summary>
        /// Toggle the window.
        /// </summary>
        public static void Toggle() {
            if (IsOpen) {
                Close();
            } else {
                Open();
            }
        }

        /// <summary>
        /// Open the window and return an instance of the window. 
        /// </summary>
        /// <param name="focus"></param> Specifies whether the window should be focused when opened.
        /// <returns></returns>
        public static LokiWindow GetWindow(bool focus = true) {
            return GetWindow<LokiWindow>(utility: true, title: "Loki", focus: focus);
        }

        /// <summary>
        /// Open the window and focus it.
        /// </summary>
        public static void Open() {
            GetWindow();
        }

        /// <summary>
        /// Close the window. 
        /// </summary>
        public new static void Close() {
            ((EditorWindow) GetWindow(focus: false)).Close();
        }

        void OnEnable() {
            this.loki = Loki.Instance;
            this.ShowOptions = EditorPrefs.GetBool("Loki Show Options", this.ShowOptions);
            this.ShowPaletteEditor = EditorPrefs.GetBool("Loki Show Palette Editor", this.ShowPaletteEditor);
            this.showAssociatedSearches = EditorPrefs.GetBool("Loki Show Associated Searches", this.showAssociatedSearches);
            this.showInlinePrefabInspector = EditorPrefs.GetBool("Loki Show Inline Prefab Inspector", this.showInlinePrefabInspector);
            this.showPlacementOptions = EditorPrefs.GetBool("Loki Show Placement Options", this.showPlacementOptions);
            this.paletteEntrySize = EditorPrefs.GetFloat("Loki Box Size", this.paletteEntrySize);
            IsOpen = true;
        }

        void OnDisable() {
            EditorPrefs.SetBool("Loki Show Options", this.ShowOptions);
            EditorPrefs.SetBool("Loki Show Palette Editor", this.ShowPaletteEditor);
            EditorPrefs.SetBool("Loki Show Associated Searches", this.showAssociatedSearches);
            EditorPrefs.SetBool("Loki Show Inline Prefab Inspector", this.showInlinePrefabInspector);
            EditorPrefs.SetBool("Loki Show Placement Options", this.showPlacementOptions);
            EditorPrefs.SetFloat("Loki Box Size", this.paletteEntrySize);
            IsOpen = false;
        }

        void OnGUI() {
            DrawUI();
            this.loki.GUIUpdate();
            foreach (var action in this.deferredActions) {
                action();
            }
            ClearDeferredActions();
        }

        void OnSelectionChange() {
            Repaint();
        }
    }
}
