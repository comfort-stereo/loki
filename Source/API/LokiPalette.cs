﻿using UnityEngine;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LokiPlugin {

    [Serializable]
    public class LokiPalette : ISerializationCallbackReceiver {
        
        [SerializeField] string label; 
        [SerializeField] bool showSearches;
        [SerializeField] List<string> addedPrefabGUIDs;
        [SerializeField] List<LokiPaletteSearch> searches;
        
        PaletteEntry[] resultCache;

        internal LokiPalette(
            string label = "Palette", 
            bool showSearches = false, 
            IEnumerable<string> addedPrefabGUIDs = null, 
            IEnumerable<LokiPaletteSearch> searches = null) 
        {
            this.label = label;
            this.showSearches = showSearches;
            this.addedPrefabGUIDs = addedPrefabGUIDs == null ? new List<string>() : addedPrefabGUIDs.ToList();
            this.searches = searches == null ? new List<LokiPaletteSearch>() : searches.ToList();
        }
        
        /// <summary>
        /// The label shown on the palette's tab.
        /// </summary>
        public string Label {
            get { return this.label; }
            set { this.label = value; }
        }
        
        /// <summary>
        /// Specifies whether the palette's searches should be displayed above the palette view for quick modification.
        /// </summary>
        public bool ShowSearches {
            get { return this.showSearches; }
            set { this.showSearches = value; }
        }
        
        /// <summary>
        /// The GUIDs of prefabs that were directly added to the palette.
        /// </summary>
        public string[] AddedPrefabGUIDs {
            get { return this.addedPrefabGUIDs.ToArray(); }
        }
        
        /// <summary>
        /// All palette searches currently applied to the palette.
        /// </summary>
        public LokiPaletteSearch[] Searches {
            get { return this.searches.ToArray(); }
            set { this.searches = value.ToList(); }
        }

        /// <summary>
        /// Clear caches. Update the palette to match the project's current state.
        /// </summary>
        public void Refresh() {
            this.resultCache = null;
        }

        /// <summary>
        /// Add a prefab to the palette by it's GUID.
        /// </summary>
        /// <param name="guid">The GUID of the prefab to add.</param>
        public void AddPrefabGUID(string guid) {
            if (this.addedPrefabGUIDs.Contains(guid)) return;
            this.addedPrefabGUIDs.Add(guid);
            Refresh();
        }

        /// <summary>
        /// Remove an added prefab from the palette by it's GUID if it is present.
        /// </summary>
        /// <param name="guid">The GUID of the prefab to remove.</param>
        /// <returns>Whether or not the prefab was removed.</returns>
        public bool RemovePrefabGUID(string guid) {
            var removed = this.addedPrefabGUIDs.Remove(guid);
            if (removed) {
                Refresh();
            }
            return removed;
        }

        /// <summary>
        /// Add a new palette search to the palette and return it.
        /// </summary>
        /// <returns>The added palette search.</returns>
        public LokiPaletteSearch AddSearch() {
            var search = new LokiPaletteSearch();
            this.searches.Add(search);
            search.OnModified += OnSearchModified;
            return search;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="search"></param>
        /// <returns>Whether or not the search was removed.</returns>
        public bool RemoveSearch(LokiPaletteSearch search) {
            var removed = this.searches.Remove(search);
            if (removed) {
                Refresh();
            }
            return removed;
        }

        internal IEnumerable<PaletteEntry> RetrieveEntries(string baseDirectory, string[] ignoredDirectories) {
            
            /* Return a cached collection of entries if possible. */ 
            if (this.resultCache != null) {
                return this.resultCache;
            }
            
            var map = new Dictionary<GameObject, PaletteEntry>();

            /* Add prefabs represented by added guids and remove those that no longer exist. */ 
            {
                var removed = new List<string>();
                
                foreach (var guid in this.addedPrefabGUIDs) {
                    var prefab = Ut.GetObjectFromGUID(guid) as GameObject;
                    if (prefab == null) {
                        removed.Add(guid);
                    } else {
                        map[prefab] = new PaletteEntry(prefab, wasAddedDirectly: true);
                    }
                }

                this.addedPrefabGUIDs.RemoveAll(guid => removed.Contains(guid));
            }


            /* Add prefabs matching the palette's searches, combining entries that point to the same object. */ 
            {
                var count = 0;
                
                foreach (var search in this.searches) {
                    
                    count++;
                    
                    if (search.IsBlank) continue;

                    foreach (var prefab in search.FindMatchingPrefabs(baseDirectory, ignoredDirectories)) {
                        PaletteEntry entry;
                        if (map.ContainsKey(prefab)) {
                            entry = map[prefab];
                        } else {
                            entry = new PaletteEntry(prefab);
                            map[prefab] = entry;
                        }
                        entry.AddSearchNumber(count);
                    }
                }
            }

            var entries = map.Values.ToArray();
            
            /* Sort the resulting entries alphabetically. */
            Array.Sort(entries, (a, b) => string.Compare(a.Prefab.name, b.Prefab.name, StringComparison.Ordinal));
            
            this.resultCache = entries;
            
            return entries;
        } 
        
        public void OnBeforeSerialize() { }

        public void OnAfterDeserialize() {
            /* Re-add the on-modified callback to each palette search. */
            foreach (var search in this.searches) {
                search.OnModified += OnSearchModified;
            }
        }

        void OnSearchModified(LokiPaletteSearch search) {
            Refresh();
        }
    }
}
