﻿using UnityEditor;
using UnityEngine;
using System.Linq;
using System.Collections.Generic;
using System.IO;
using System;

namespace LokiPlugin {
    
    public partial class LokiWindow {

        void DrawUI() {
            
            if (Event.current.type == EventType.Repaint) {
                this.repainting = true;
            } else if (Event.current.type == EventType.Layout) {
                this.repainting = false;
            }
            
            EditorGUILayout.BeginHorizontal();
            {
                EditorGUILayout.BeginVertical();
                {
                    DrawPaletteTabs();
                    DrawSmallPaletteEditorSearches();
                    if (this.showInlinePrefabInspector) {
                        DrawInlinePrefabInspector();
                    }
                    DrawPalette();
                    DrawGridSizeSelector();
                    DrawPlacementOptions();
                    DrawFoldoutButtons();
                }
                EditorGUILayout.EndVertical();
                if (this.ShowPaletteEditor) {
                    DrawPaletteEditor();
                }
                if (this.ShowOptions) {
                    DrawOptions();
                }
            }
            EditorGUILayout.EndHorizontal();
        }

        void DrawPaletteTabs() {
            DrawHeader("Palette");
            
            EditorGUILayout.BeginHorizontal();
            {
                var labels = this.loki.Palettes.Select(x => x.Label).ToArray();
                this.loki.SelectedPaletteIndex = GUILayout.SelectionGrid(
                    this.loki.SelectedPaletteIndex, labels, PaletteTabsPerRow, GUILayout.MinWidth(MinimumWidth)
                );
            }
            EditorGUILayout.EndHorizontal();
        }

        void DrawPaletteSearch(LokiPaletteSearch search, bool inPaletteEditor, int number) {
            if (inPaletteEditor) {
                EditorGUILayout.LabelField(number.ToString(), Style.LargeLabelCenter);
            }
            
            search.OnlyPrefabsInScene = EditorGUILayout.Toggle("Only Prefabs in Scene", search.OnlyPrefabsInScene);
            
            search.ComponentField = EditorGUILayout.TextField(
                (search.ComponentField.Length == 0 || search.ComponentField == "GameObject" || Ut.ComponentExists(search.ComponentField)) ? 
                    "Component" : "Component (DNE)", search.ComponentField
            );
            search.TagField = EditorGUILayout.TextField(
                (search.TagField.Length == 0 || Ut.TagExists(search.TagField)) ? "Tag" : "Tag (DNE)", search.TagField
            );
            search.LayerField = EditorGUILayout.TextField(
                (search.LayerField.Length == 0 || Ut.LayerExists(search.LayerField)) ? "Layer" : "Layer (DNE)", search.LayerField
            ); 
            
            search.NameField = EditorGUILayout.TextField("Name", search.NameField);

            search.Directory = DrawDirectoryField("Directory", search.Directory);
        }

        void DrawPaletteSearches(LokiPalette palette, bool inPaletteEditor) {
            if (palette.Searches.Length == 0 && !inPaletteEditor) return;

            if (inPaletteEditor) {
                EditorGUILayout.LabelField("Searches", Style.LargeLabelCenter);
            }

            if (inPaletteEditor) {
                this.paletteSearchesScroll = EditorGUILayout.BeginScrollView(this.paletteSearchesScroll, Style.Enclosed);
            } else {
                this.smallPaletteEditorSearchesScroll = EditorGUILayout.BeginScrollView(
                    this.smallPaletteEditorSearchesScroll, Style.Enclosed, GUILayout.MinHeight(PaletteSearchHeight)
                );
            }
            {
                var count = 0;
                foreach (var search in palette.Searches) {
                    try {
                        EditorGUILayout.BeginVertical(Style.Enclosed);
                        {
                            DrawPaletteSearch(search, inPaletteEditor, ++count);
                            if (inPaletteEditor) {
                                if (Button("Remove")) {
                                    palette.RemoveSearch(search);
                                    break;
                                }
                            }
                        }
                        EditorGUILayout.EndVertical();
                    } catch (ArgumentException) {
                        /* Ignore drawing error when adding folders by drag and drop. */
                    }
                }
            }
            EditorGUILayout.EndScrollView();
        }

        string DrawDirectoryField(string label, string current, bool settable = true, string cleared = null) {
            
            if (settable) {
                EditorGUILayout.BeginHorizontal();
                {
                    if (label.Length != 0) {
                        GUILayout.Label(label, GUILayout.Width(146));
                    }
                    if (Button(" Set ")) {
                        return Ut.RelativeAssetDirectoryPathDialog();
                    }

                    EditorGUI.BeginDisabledGroup(current.Length == 0);
                    {
                        if (Button("Clear")) {
                            return cleared ?? "";
                        }
                    }
                    EditorGUI.EndDisabledGroup();
                }
                EditorGUILayout.EndHorizontal();
            }


            if (settable) {
                EditorGUILayout.BeginHorizontal(Style.Enclosed);
            } else {
                EditorGUILayout.BeginHorizontal();
            }
            {
                string path; 
                if (current.Length == 0) {
                    path = "No Path";
                } else if (Directory.Exists(current)) {
                    path = current;
                } else {
                    path = "(Invalid Directory) " + current;
                }

                EditorGUILayout.LabelField(path);

                if (!settable) {
                    if (Button("Remove")) {
                        return "";
                    }
                }
            }
            EditorGUILayout.EndHorizontal();
            
            return current;
        }

        void DrawOptions() {
            EditorGUILayout.BeginVertical(Style.Enclosed, GUILayout.Width(FoldoutWindowLayoutWidth));
            {
                DrawHeader("Options");
                EditorGUILayout.BeginVertical();
                {
                    DrawUIOptions();
                    DrawSearchOptions();
                    DrawKeybindingOptions();
                }
                EditorGUILayout.EndVertical();
            }
            EditorGUILayout.EndVertical();
        }

        void DrawSearchOptions() {
            EditorGUILayout.LabelField("Search", Style.LargeLabelCenter);
            
            EditorGUILayout.BeginVertical(Style.Enclosed);
            {
                EditorGUILayout.LabelField("Base Prefab Directory", Style.LargeLabelCenter);
                this.loki.BasePrefabDirectory = DrawDirectoryField("", this.loki.BasePrefabDirectory, cleared: Loki.DefaultBasePrefabDirectory);

                EditorGUILayout.LabelField("Ignored Directories", Style.LargeLabelCenter);
                    
                var ignoredDirectories = this.loki.IgnoredPrefabDirectories;
                
                foreach (var directory in ignoredDirectories) {
                    var changed = DrawDirectoryField("", directory, settable: false);
                    if (changed.Length == 0) {
                        this.loki.RemoveIgnoredPrefabDirectory(directory);
                    } else if (changed != directory) {
                        this.loki.RemoveIgnoredPrefabDirectory(directory);
                        this.loki.AddIgnoredPrefabDirectory(changed);
                    }
                }
                
                if (Button("Add Ignored Directory")) {
                    var ignored = Ut.RelativeAssetDirectoryPathDialog();
                    this.loki.AddIgnoredPrefabDirectory(ignored);
                }
            }
            EditorGUILayout.EndVertical();
        }

        void DrawUIOptions() {
            EditorGUILayout.LabelField("UI", Style.LargeLabelCenter);
            EditorGUILayout.BeginVertical(Style.Enclosed);
            {
                this.loki.HighlightColor = EditorGUILayout.ColorField("Highlight Color", this.loki.HighlightColor);
                this.loki.GridColor = EditorGUILayout.ColorField("Grid Color", this.loki.GridColor);
                this.loki.GridPlacementOverlapColor = EditorGUILayout.ColorField("Overlap Color", this.loki.GridPlacementOverlapColor);
                this.paletteEntrySize = Mathf.Round(
                    EditorGUILayout.Slider("Palette Entry Size", this.paletteEntrySize, MinimumPaletteEntrySize, MaximumPaletteEntrySize)
                );
                this.showAssociatedSearches = EditorGUILayout.ToggleLeft("Show Associated Searches In Palette", this.showAssociatedSearches);
                this.showInlinePrefabInspector = EditorGUILayout.ToggleLeft("Show Prefab Inspector Header", this.showInlinePrefabInspector);
                this.loki.AlwaysShowGrid = EditorGUILayout.ToggleLeft("Always Show Grid", this.loki.AlwaysShowGrid);
                this.loki.HideUnityGridWhenGridIsShown = EditorGUILayout.ToggleLeft(
                    "Hide Unity Grid When Grid Is Shown", this.loki.HideUnityGridWhenGridIsShown
                );
                this.loki.CloseWindowWhenPlacingPrefab = EditorGUILayout.ToggleLeft(
                    "Close Window When Placing Prefab", this.loki.CloseWindowWhenPlacingPrefab
                );
            }
            EditorGUILayout.EndVertical();
        }

        void DrawKeybindingOptions() {
            EditorGUILayout.LabelField("Keybindings", Style.LargeLabelCenter);
            this.keybindingsScroll = EditorGUILayout.BeginScrollView(this.keybindingsScroll, Style.Enclosed);
            {
                KeybindingField("Grab Object(s)", this.loki.KeybindingGrabGameObjects);
                KeybindingField("Duplicate And Grab Object(s)", this.loki.KeybindingDuplicateAndGrabGameObjects);
                this.loki.KeybindingModifiersRotate = KeybindingModifiersField(
                    "Rotate Object(s)", this.loki.KeybindingModifiersRotate
                );
                KeybindingField("Reset", this.loki.KeybindingReset);
                KeybindingField("Toggle Grid Lock", this.loki.KeybindingToggleGridLock);
                KeybindingField("Toggle Window", this.loki.KeybindingToggleWindow);
                KeybindingField("Select Grid Size Up", this.loki.KeybindingSelectGridSizeUp);
                KeybindingField("Select Grid Size Down", this.loki.KeybindingSelectGridSizeDown);
            }
            EditorGUILayout.EndScrollView();

            if (Button("Close Options")) {
                this.ShowOptions = false;
            }
        }

        void DrawPaletteEditor() {
            var palette = this.loki.SelectedPalette;

            EditorGUILayout.BeginVertical(Style.Enclosed, GUILayout.Width(FoldoutWindowLayoutWidth));
            {
                DrawHeader("Palette Editor");

                EditorGUILayout.BeginVertical();
                {

                    palette.Label = EditorGUILayout.TextField("Palette Label", palette.Label);
                    palette.ShowSearches = EditorGUILayout.ToggleLeft("Show Searches Above Palette", palette.ShowSearches);

                    DrawPaletteSearches(palette, inPaletteEditor: true);

                    if (Button("Add Search To Palette")) {
                        palette.AddSearch();
                    }

                    EditorGUILayout.BeginHorizontal();
                    {
                        if (Button("Add Palette")) {
                            this.loki.AddPalette();
                        }
                        EditorGUI.BeginDisabledGroup(this.loki.Palettes.Length < 2);
                        {
                            if (Button("Delete Palette")) {
                                this.loki.RemoveSelectedPalette();
                            }
                        }
                        EditorGUI.EndDisabledGroup();
                    }
                    EditorGUILayout.EndHorizontal();
                    if (Button("Close Palette Editor")) {
                        this.ShowPaletteEditor = false;
                    }
                }
                EditorGUILayout.EndVertical();
            }
            EditorGUILayout.EndVertical();
        } 

        void DrawSmallPaletteEditorSearches() {
            var palette = this.loki.SelectedPalette;
            if (palette.ShowSearches) {
                DrawPaletteSearches(palette, inPaletteEditor: false);
            }
        }

        float DeterminePaletteWidth() {
            var width = this.position.width - this.TotalPaletteEntrySize - PaletteEntryBorderSize;
            if (this.ShowOptions) {
                width -= FoldoutWindowWidth;
            }
            if (this.ShowPaletteEditor) {
                width -= FoldoutWindowWidth;
            }
            return width;
        }

        void DrawPalette() {
            
            var palette = this.loki.SelectedPalette;
            var entries = palette.RetrieveEntries(this.loki.BasePrefabDirectory, this.loki.IgnoredPrefabDirectories);
            var width = DeterminePaletteWidth();
            var columns = Mathf.Max(0, (int) width / (int) this.TotalPaletteEntrySize);
            
            this.paletteScrollPosition = EditorGUILayout.BeginScrollView(this.paletteScrollPosition, Style.Enclosed);
            {
                var input = Event.current;
                
                HandlePaletteDragAndDrop(input);
                HandlePaletteMouseDragSelection(input);
                
                var row = 0;
                var column = 0;

                foreach (var entry in entries) {

                    if (column > columns) {
                        GUILayoutUtility.GetRect(width, this.paletteEntrySize);
                        column = 0;
                        row++;
                    }

                    var position = new Vector2(
                        PaletteEntryBorderSize + column * this.TotalPaletteEntrySize, 
                        PaletteEntryBorderSize + row * this.TotalPaletteEntrySize
                    );

                    var area = new Rect(position.x, position.y, this.paletteEntrySize, this.paletteEntrySize);
                    DrawPaletteEntry(entry, area);

                    column++;
                }
                
                if (this.mouseDragPositionStart != null) {
                    DrawMouseDragSelectionBox(input);
                }
            }
            EditorGUILayout.EndScrollView();
        }

        void DrawPaletteEntry(PaletteEntry entry, Rect area) {

            Color color;

            if (entry.Prefab == this.loki.PlacementPrefab) {
                color = this.loki.HighlightColor;
            } else if (this.loki.HasPrefabSelected(entry.Prefab)) {
                color = this.loki.HighlightColor;
                color.a = 0.3f;
            } else {
                color = this.paletteEntryUnselectedColor;
            }
            
            EditorGUI.DrawRect(area, color);

            var preview = AssetPreview.GetAssetPreview(entry.Prefab);
            if (preview != null) {
                var previewArea = new Rect(
                    area.x + 0.5f * area.width * (1f - PaletteEntryInsetFactor),
                    area.y + 0.5f * area.height * (1f - PaletteEntryInsetFactor),
                    area.width * PaletteEntryInsetFactor, 
                    area.height * PaletteEntryInsetFactor 
                );
                EditorGUI.DrawPreviewTexture(previewArea, preview);
            }

            var labelArea = new Rect(area.x, area.y + PaletteEntryLabelY, area.width, area.height);
            var symbolArea = new Rect(area.x - this.paletteEntrySize / 2f - 6f, area.y, area.width, area.height);
            
            GUI.Label(labelArea, entry.Prefab.name, Style.PaletteEntryLabelStyle);

            if (entry.WasAddedDirectly) {
                GUI.Label(symbolArea, "+", Style.PaletteEntryLabelStyle);
                symbolArea.y += PaletteEntrySymbolDeltaY;
            }

            if (this.showAssociatedSearches) {
                var numbers = entry.SearchNumbers;
                for (var i = 0; i < numbers.Length; i++) {
                    GUI.Label(symbolArea, numbers[i].ToString(), Style.PaletteEntryLabelStyle);
                    symbolArea.y += PaletteEntrySymbolDeltaY;
                }
            }

            HandlePaletteEntryInteractions(entry, area);
        }

        void HandlePaletteMouseDragSelection(Event input) {
            if (this.loki.IsReplacingPrefabInstances) return;
            if (input.type == EventType.MouseDrag && input.button == 0 && this.mouseDragPositionStart == null) {
                this.mouseDragPositionStart = input.mousePosition;
            } else if (input.type == EventType.MouseUp && input.button == 0) {
                this.mouseDragPositionStart = null;
            }
        }

        void HandlePaletteDragAndDrop(Event input) {

            if (input.type != EventType.DragUpdated && input.type != EventType.DragPerform) return;
            
            var paths = DragAndDrop.paths;
            if (paths.Length == 0) return;

            /* Handle dropping of folders. */
            {
                if (paths.All(Directory.Exists)) {
                    DragAndDrop.AcceptDrag();
                    DragAndDrop.visualMode = DragAndDropVisualMode.Link;

                    if (input.type == EventType.DragPerform) {
                        DeferAction(() => {
                            var palette = this.loki.SelectedPalette;
                            foreach (var path in paths) {
                                if (palette.Searches.All(search => search.Directory != path)) {
                                    var search = palette.AddSearch();
                                    search.Directory = path;
                                }
                            }
                            this.ShowPaletteEditor = true;
                            Repaint();
                        });
                    }
                    return;
                }
            }

            /* Handle dropping of prefabs. */
            {
                var objects = DragAndDrop.objectReferences.OfType<GameObject>().Where(Ut.IsPrefab).ToArray();

                if (objects.Length != 0) {
                    DragAndDrop.AcceptDrag();
                    DragAndDrop.visualMode = DragAndDropVisualMode.Link;

                    DeferAction(() => {
                        if (input.type == EventType.DragPerform) {
                            foreach (var obj in objects) {
                                this.loki.AddPrefabGUIDToPaletteDirectly(Ut.GetGUID(obj));
                            }
                            Repaint();
                        }
                    });
                }
            }
        }

        void HandlePaletteEntryInteractions(PaletteEntry entry, Rect area) {
            
            var input = Event.current;
            
            var mouseIsInArea = area.Contains(input.mousePosition);

            if (this.loki.IsReplacingPrefabInstances) {
                Ut.SetEditorCursor(MouseCursor.Link);
                if (mouseIsInArea) {
                    if (input.type == EventType.MouseDown && input.button == 0) {
                        this.loki.ReplaceInstancesOfSelectedPrefabsWith(entry.Prefab);
                    }
                }
                return;
            }
            
            if (this.mouseDragPositionStart != null) {
                var box = GetMouseDragSelectBox(input);
                var isSelected = this.loki.SelectedPrefabs.Contains(entry.Prefab);
                var isOverlapped = box.Overlaps(area, true);

                if (this.repainting) {
                    if (isOverlapped) {
                        if (!isSelected) {
                            this.loki.SelectPrefabAdditive(entry.Prefab);
                        }
                        return;
                    } 
                    if (isSelected) {
                        if (!input.shift) {
                            this.loki.DeselectPrefab(entry.Prefab);
                        }
                    }
                }
                Repaint();
                return;
            }
            
            if (input.type == EventType.MouseDown) {
                if (mouseIsInArea) {
                    if (input.button == 0) {
                        if (this.loki.HasPrefabSelected(entry.Prefab) || this.loki.PlacementPrefab == entry.Prefab) {
                            this.loki.BeginPlacingPrefab(entry.Prefab);
                        } else {
                            if (this.loki.PlacementPrefab == null) {
                                if (input.shift) {
                                    this.loki.SelectPrefabAdditive(entry.Prefab);
                                } else {
                                    this.loki.SelectPrefab(entry.Prefab);
                                }
                            }
                        }
                    } else if (input.button == 1 && !this.loki.IsReplacingPrefabInstances) {
                        this.loki.SelectPrefabAdditive(entry.Prefab);
                        ShowPaletteEntryContextMenu();
                    }
                } else {
                    if (input.button == 0 && !input.shift) {
                        this.loki.DeselectPrefab(entry.Prefab);
                    }
                }
                Repaint();
            }
        }
        
        void ShowPaletteEntryContextMenu() {
            var menu = new GenericMenu();
            
            if (this.loki.CanRemoveSelectedPrefabsFromPalette()) {
                menu.AddItem(new GUIContent("Remove From Palette"), false, this.loki.RemoveSelectedPrefabsFromPalette);
            } else {
                menu.AddDisabledItem(new GUIContent("Remove From Palette (Added By Search)"));
            }
            
            if (this.loki.CanAddSelectedPrefabsToPaletteDirectly()) {
                menu.AddItem(new GUIContent("Add To Palette Directly"), false, this.loki.AddSelectedPrefabsToPaletteDirectly);
            } else {
                menu.AddDisabledItem(new GUIContent("Add To Palette Directly (Already Added)"));
            }
            
            menu.AddSeparator("");
            
            menu.AddItem(new GUIContent("Reveal Location"), false, this.loki.RevealSelectedPrefabLocation);
            
            menu.AddSeparator("");
            
            if (this.loki.SelectedPrefabsHaveInstance()) {
                menu.AddItem(new GUIContent("Select Instances"), false, this.loki.SelectInstancesOfSelectedPrefabs);
                menu.AddItem(new GUIContent("Delete Instances (Can Undo)"), false, this.loki.DeleteInstancesOfSelectedPrefabs);
                menu.AddItem(new GUIContent("Replace Instances (Can Undo)"), false, this.loki.BeginReplaceInstancesOfSelectedPrefabs);
                menu.AddItem(new GUIContent("Revert Instances Scene (Can Undo, Position and Name Preserved)"), false, this.loki.RevertInstancesOfSelectedPrefabs);
            } else {
                menu.AddDisabledItem(new GUIContent("Select Instances In Scene (No Instances)"));
                menu.AddDisabledItem(new GUIContent("Delete Instances In Scene (No Instances)"));
                menu.AddDisabledItem(new GUIContent("Replace Instances In Scene (No Instances)"));
                menu.AddDisabledItem(new GUIContent("Revert Instances In Scene (No Instances)"));
            }
            
            menu.AddSeparator("");

            if (this.loki.CanReplaceSelectedGameObjectsWithSelectedPrefab()) {
                menu.AddItem(new GUIContent("Replace Selected Game Objects With This (Can Undo)"), false, this.loki.ReplaceSelectedGameObjectsWithSelectedPrefab);
            } else {
                menu.AddDisabledItem(new GUIContent("Replace Selected Game Objects With This (None Selected)"));
            }
            
            menu.AddSeparator("");
            
            menu.AddItem(new GUIContent("Delete Prefab(s) (Prompt)"), false, this.loki.PromptToDeleteSelectedPrefabs);
            menu.ShowAsContext();
        }

        void DrawInlinePrefabInspector() {
            try {
                var selected = this.loki.SelectedPrefabs.Select(obj => (UnityEngine.Object) obj).ToArray();
                if (selected.Length != 0 && !this.ShowPaletteEditor && !this.ShowOptions) {
                    var editor = Editor.CreateEditor(selected);
                    editor.DrawHeader();
                } else {
                    EditorGUILayout.BeginVertical(Style.Enclosed);
                    {
                        GUILayoutUtility.GetRect(0f, 43f);
                    }
                    EditorGUILayout.EndVertical();
                }
            } catch (Exception) { }
        }

        void DrawGridSizeSelector() {
            DrawHeader("Grid Size");
            EditorGUILayout.BeginHorizontal(Style.Enclosed);
            {
                var sizes = this.loki.GridSizes;
                var sizeBoxes = sizes.Select(x => x.ToString()).ToArray();
                this.loki.SelectedGridSizeIndex = GUILayout.SelectionGrid(
                    this.loki.SelectedGridSizeIndex, sizeBoxes, GridSizesPerRow, GUILayout.MinWidth(MinimumWidth)
                );
            }
            EditorGUILayout.EndHorizontal();
        }

        void DrawPlacementOptions() {
            if (Button(this.showPlacementOptions ? "Close Placement" : "Placement")) {
                this.showPlacementOptions = !this.showPlacementOptions;
            }

            if (!this.showPlacementOptions) return;
            
            EditorGUILayout.BeginVertical(Style.Enclosed);
            {
                this.loki.RotationDelta = EditorGUILayout.FloatField("Rotation Delta", this.loki.RotationDelta);
                this.loki.GridLockMode = (GridLockMode)EditorGUILayout.EnumPopup("Grid Lock Mode", this.loki.GridLockMode);

                EditorGUILayout.BeginHorizontal();
                {
                    if (this.addingNewGridSize) {
                        this.enteredGridSize = EditorGUILayout.DelayedFloatField(this.enteredGridSize);
                        if (this.enteredGridSize > 0) {
                            this.loki.AddGridSize(this.enteredGridSize);
                            this.enteredGridSize = 0;
                            this.addingNewGridSize = false;
                        }
                        if (Button("Cancel") || Event.current.keyCode == KeyCode.Escape) {
                            this.addingNewGridSize = false;
                        }
                    } else {
                        if (Button("Add Grid Size", "Add a new grid size to select from.")) {
                            this.addingNewGridSize = true;
                        }
                        EditorGUI.BeginDisabledGroup(this.loki.GridSizes.Length < 2);
                        {
                            if (Button("Remove Grid Size", "Remove the currently selected grid size.")) {
                                this.loki.RemoveSelectedGridSize();
                            }
                        }
                        EditorGUI.EndDisabledGroup();
                    }
                }
                EditorGUILayout.EndHorizontal();
            }
            EditorGUILayout.EndVertical();
        }

        void DrawFoldoutButtons() {
            EditorGUILayout.BeginVertical();
            {
                if (Button(this.ShowOptions ? "Close Options" : "Options")) {
                    this.ShowOptions = !this.ShowOptions;
                }
                if (Button(this.ShowPaletteEditor ? "Close Palette Editor" : "Palette Editor")) {
                    this.ShowPaletteEditor = !this.ShowPaletteEditor;
                }
            }
            EditorGUILayout.EndVertical();
        }
        
        Rect GetMouseDragSelectBox(Event input) {
            if (this.mouseDragPositionStart == null) {
                return Rect.zero;
            }
            var start = this.mouseDragPositionStart.Value;
            var size = input.mousePosition - start; 
            return new Rect(start, size);
        }

        void DrawMouseDragSelectionBox(Event input) {
            var box = GetMouseDragSelectBox(input);
            var previousColor = GUI.backgroundColor;
            var color = this.loki.HighlightColor;
            color.a = 0.15f;
            GUI.backgroundColor = color;
            GUI.Box(box.AsPositive(), "");
            GUI.backgroundColor = previousColor;
        }


        void DrawHeader(string label) {
            EditorGUILayout.BeginVertical(Style.Enclosed);
            {
                EditorGUILayout.LabelField(label, Style.Header, GUILayout.MinWidth(MinimumWidth));
            }
            EditorGUILayout.EndVertical();
        }

        bool Button(string name, string tooltip = "") {
            if (tooltip == "") {
                return GUILayout.Button(new GUIContent(name));
            } 
            return GUILayout.Button(new GUIContent(name, tooltip));
        }

        void KeybindingField(string label, LokiKeybinding keybinding) {
            EditorGUILayout.BeginVertical();
            {
                EditorGUILayout.LabelField(label, Style.BoldLabelLeft);
                keybinding.KeyCode = (KeyCode) EditorGUILayout.EnumPopup("Key", keybinding.KeyCode);
                keybinding.Modifiers = EventModifiersField("Modifiers", keybinding.Modifiers);
                keybinding.RequiresSceneFocus = EditorGUILayout.Toggle("Requires Scene Focus", keybinding.RequiresSceneFocus);
            }
            EditorGUILayout.EndVertical();
        }

        EventModifiers KeybindingModifiersField(string label, EventModifiers modifiers) {
            EventModifiers returned; 
            EditorGUILayout.BeginVertical();
            {
                EditorGUILayout.LabelField(label, Style.BoldLabelLeft);
                returned = EventModifiersField("Modifiers", modifiers);
            }
            EditorGUILayout.EndVertical();
            return returned;
        }

        EventModifiers EventModifiersField(string label, EventModifiers current) {
            return (EventModifiers)((int)(EventModifiers) EditorGUILayout.EnumMaskField(label, (EventModifiers)((int)(current) << 1)) >> 1);
        }
    }
}