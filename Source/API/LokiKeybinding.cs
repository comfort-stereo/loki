using UnityEngine;
using System;

namespace LokiPlugin {
    
    [Serializable]
    public class LokiKeybinding {
        [SerializeField] KeyCode keyCode;
        [SerializeField] EventModifiers modifiers;
        [SerializeField] bool requiresSceneFocus;

        public LokiKeybinding(KeyCode keyCode, EventModifiers modifiers = EventModifiers.None, bool requiresSceneFocus = false) {
            this.keyCode = keyCode;
            this.modifiers = modifiers;
            this.requiresSceneFocus = requiresSceneFocus;
        }

        public KeyCode KeyCode {
            get { return this.keyCode; }
            set { this.keyCode = value; }
        }

        public EventModifiers Modifiers {
            get { return this.modifiers; }
            set { this.modifiers = value; }
        }

        public bool RequiresSceneFocus {
            get { return this.requiresSceneFocus; }
            set { this.requiresSceneFocus = value; }
        }
        
        public bool Matches(Event input) {
            return input != null &&
                   input.keyCode == this.keyCode &&
                   input.modifiers == this.modifiers &&
                   input.rawType == EventType.KeyDown;
        }
    }
}