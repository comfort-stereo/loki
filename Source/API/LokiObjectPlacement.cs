using System.Linq;
using UnityEditor;
using UnityEngine;
using System.Collections.Generic;

namespace LokiPlugin {
    public partial class Loki {

        /// <summary>
        /// Grab selected game objects in the scene and move them along with the cursor.
        /// </summary>
        public void GrabSelectedGameObjects() {
            if (Selection.activeGameObject != null && this.PlacementPrefab == null) {
                var objects = Selection.gameObjects;

                var anchor = objects.First();
                var maxX = anchor.transform.position.x;
                var minY = anchor.transform.position.y;

                foreach (var other in objects) {
                    var position = other.transform.position;
                    if (position.x > maxX || position.y < minY) {
                        maxX = position.x;
                        minY = position.y;
                        anchor = other;
                    } 
                }

                var entries = objects.Select(obj => new MovedGameObjectEntry {
                    GameObject = obj,
                    InitialPosition = obj.transform.position,
                    InitialRotation = obj.transform.eulerAngles,
                    PositionOffsetFromAnchorObject = obj.transform.position - anchor.transform.position
                });

                this.MovedGameObjectEntries.AddRange(entries);
                
                foreach (var obj in objects) {
                    if (this.OnBeginPlacingGameObject != null) {
                        this.OnBeginPlacingGameObject(obj, false);
                    }
                }
                
                Undo.RegisterCompleteObjectUndo(objects.OfType<Object>().ToArray(), "Grabbed Objects");
                this.lastToolUsed = Tools.current;
            }
        }
        
        void HandleObjectPlacement(Event input, SceneView view) {
            
            GizmoDrawer.Instance.ClearGridSpaceIndicators();

            var moved = this.MovedGameObjectEntries;
            
            if (moved.Count == 0) {
                this.lastToolUsed = Tools.current;
                return;
            } 
            
            var rotating = input.modifiers == this.data.KeybindingModifiersRotate;
            if (rotating) {
                Tools.current = Tool.Rotate;
            } else { 
                Tools.current = Tool.View;
            } 

            foreach (var entry in moved) {
                var obj = entry.GameObject;
                
                MoveGameObjectToMousePosition(view, obj, this.GridLockEnabled);
                obj.transform.position += entry.PositionOffsetFromAnchorObject;

                var overlaps = FindHomogeneousGameObjectsAtPosition(obj, obj.transform.position).Length != 0;
                var color = overlaps ? this.data.GridPlacementOverlapColor : this.data.GridColor; 
                
                GizmoDrawer.Instance.ShowGridSpaceIndicator(obj.transform.position, color, this.SelectedGridSize);
            }

            var placingPrefab = this.placementPrefab != null;
            
            if (input.type == EventType.MouseUp || input.type == EventType.MouseDrag && this.GridLockEnabled) {
                if (input.button == 0) {
                    if (this.objectPlacementsToIgnore > 0) {
                        this.objectPlacementsToIgnore--;
                        return;
                    } 
                    foreach (var entry in moved) {
                        var obj = entry.GameObject;
                        var placed = placingPrefab ? this.placementPrefab : obj;
                        PlaceGameObject(placed, obj.transform.position, obj.transform.eulerAngles, placingPrefab);
                        if (placingPrefab && input.type == EventType.MouseDrag) {
                            input.Use();
                        }
                    }
                    if (!placingPrefab) {
                        Reset(false);
                    }
                }
            }
            

            if (input.type == EventType.MouseUp || input.type == EventType.MouseDrag && this.GridLockEnabled) {
                if (input.button == 1) {
                    EraseGameObjects();
                    input.Use();
                }
            }

            if (input.type == EventType.MouseUp && input.button == 1) {
                foreach (var entry in moved) {
                    entry.GameObject.transform.eulerAngles = Vector3.zero;
                }
            }
            
            if (input.isScrollWheel && rotating) {
                foreach (var entry in moved) {
                    entry.GameObject.transform.Rotate(Vector3.forward * (input.delta.y / 3) * this.RotationDelta);
                }
                input.Use();
            }
        }

        GameObject PlaceGameObject(GameObject obj, Vector3 position, Vector3 rotation, bool isPlacementPrefab) {
            if (this.OnCanPlaceGameObject != null) {
                var results = this.OnCanPlaceGameObject.InvokeAll<bool>(obj, position, rotation, isPlacementPrefab);
                if (results.Contains(false)) {
                    return null;
                }
            }
            
            foreach (var other in FindHomogeneousGameObjectsAtPosition(obj, position)) {
                Undo.DestroyObjectImmediate(other);
            }

            var placed = isPlacementPrefab ? Ut.Copy(obj) : obj;
            
            placed.transform.position = position;
            placed.transform.eulerAngles = rotation;

            if (this.OnGameObjectPlaced != null) {
                this.OnGameObjectPlaced(placed, position, rotation, isPlacementPrefab);
            }

            if (isPlacementPrefab) {
                Undo.RegisterCreatedObjectUndo(placed, "Placed Prefab");
            }

            return placed;
        }

        GameObject[] FindHomogeneousGameObjectsAtPosition(GameObject obj, Vector3 position) {
            var moved = this.MovedGameObjectEntries;
            List<GameObject> found = null; 
            
            foreach (var entry in moved) {
                var objects = Ut.FindGameObjectsWithinDistance(position, SamePositionFudgeFactor * this.SelectedGridSize);
                foreach (var other in objects) {
                    if (entry.GameObject == other) continue;
                    if (moved.Any(x => x.GameObject == other)) continue;
                    if (this.homogeneousComparison(obj, other)) {
                        if (found == null) {
                            found = new List<GameObject>();
                        }
                        found.Add(other);
                    }
                }
            }
            
            if (found == null) {
                return Arrays<GameObject>.Empty;
            }
            
            return found.ToArray();
        }

        void EraseGameObjects() {
            foreach (var entry in this.MovedGameObjectEntries) {
                foreach (var other in FindHomogeneousGameObjectsAtPosition(entry.GameObject, entry.GameObject.transform.position)) {
                    Undo.DestroyObjectImmediate(other);
                }
            }
        }

        void MoveGameObjectToMousePosition(SceneView view, GameObject obj, bool byGrid) {
            var mouse = Event.current.mousePosition;
            mouse.y = Camera.current.pixelHeight - mouse.y;
            var position = view.camera.ScreenToWorldPoint(mouse);
            position.z = 0;
            obj.transform.position = byGrid ? GridUtility.MovePositionToGrid(position, this.GridLockMode, this.SelectedGridSize) : position;
        }
    }
}