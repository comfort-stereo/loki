﻿using UnityEngine;
using UnityEditor;
using System;
using System.Collections.Generic;

namespace LokiPlugin {

    [Serializable]
    public class LokiPaletteSearch {
        
        [SerializeField] string componentField;
        [SerializeField] string nameField;
        [SerializeField] string tagField;
        [SerializeField] string layerField;
        [SerializeField] string directory;
        [SerializeField] bool onlyPrefabsInScene;

        public LokiPaletteSearch(
            string componentField = "", 
            string nameField = "", 
            string tagField = "", 
            string layerField = "", 
            string directory = "", 
            bool onlyPrefabsInScene = false
        ) {
            this.componentField = componentField;
            this.nameField = nameField;
            this.tagField = tagField;
            this.layerField = layerField;
            this.directory = directory;
            this.onlyPrefabsInScene = onlyPrefabsInScene;
        }

        internal delegate void OnModifiedAction(LokiPaletteSearch search);
        internal event OnModifiedAction OnModified; 

        public string ComponentField {
            get { return this.componentField; }
            set {
                if (this.componentField != value && this.OnModified != null) {
                    this.OnModified(this);
                }
                this.componentField = value;
            }
        }

        public string NameField {
            get { return this.nameField; }
            set {
                if (this.nameField != value && this.OnModified != null) {
                    this.OnModified(this);
                }
                this.nameField = value;
            }
        }

        public string TagField {
            get { return this.tagField; }
            set {
                if (this.tagField != value && this.OnModified != null) {
                    this.OnModified(this);
                }
                this.tagField = value;
            }
        }
        
        public string LayerField {
            get { return this.layerField; }
            set {
                if (this.layerField != value && this.OnModified != null) {
                    this.OnModified(this);
                }
                this.layerField = value;
            }
        }

        public string Directory {
            get { return this.directory; }
            set {
                if (this.directory != value && this.OnModified != null) {
                    this.OnModified(this);
                }
                this.directory = value;
            }
        }

        public bool OnlyPrefabsInScene {
            get { return this.onlyPrefabsInScene; }
            set {
                if (this.onlyPrefabsInScene != value && this.OnModified != null) {
                    this.OnModified(this);
                }
                this.onlyPrefabsInScene = value;
            }
        }

        public bool IsBlank {
            get {
                return 
                    this.componentField.Length == 0 && 
                    this.nameField.Length == 0 && 
                    this.tagField.Length == 0 && 
                    this.layerField.Length == 0 && 
                    this.directory.Length == 0;
            }
        }

        public GameObject[] FindMatchingPrefabs(string baseDirectory, string[] ignoredDirectories) {
            if (this.IsBlank) {
                return Arrays<GameObject>.Empty; 
            }

            var prefabs = this.onlyPrefabsInScene 
                ? Ut.FindUniquePrefabsInScene() 
                : Ut.FindPrefabsInProject(baseDirectory, ignoredDirectories);

            var checkComponent = this.componentField.Length != 0 && this.componentField != "GameObject";
            var checkName = this.nameField.Length != 0;
            var checkTag = this.tagField.Length != 0;
            var checkLayer = this.layerField.Length != 0;
            var checkDirectory = this.directory.Length != 0;

            Type component = null;
            if (checkComponent) {
                if ((component = Ut.GetComponentType(this.componentField)) == null) {
                    return Arrays<GameObject>.Empty;
                }
            }

            if (checkTag && !Ut.TagExists(this.tagField)) {
                return Arrays<GameObject>.Empty;
            }

            if (checkLayer && !Ut.LayerExists(this.layerField)) {
                return Arrays<GameObject>.Empty;
            }

            if (checkDirectory && !System.IO.Directory.Exists(this.directory)) {
                return Arrays<GameObject>.Empty;
            }

            var results = new List<GameObject>(prefabs.Length);
            
            var nameField = this.nameField.ToLower();

            foreach (var prefab in prefabs) {
                if (checkComponent && prefab.GetComponent(component) == null) continue;
                if (checkName && !prefab.name.ToLower().StartsWith(nameField)) continue;
                if (checkLayer && LayerMask.LayerToName(prefab.layer) != this.layerField) continue;
                if (checkTag && !prefab.CompareTag(this.tagField)) continue;
                if (checkDirectory && !AssetDatabase.GetAssetPath(prefab.GetInstanceID()).StartsWith(this.directory)) continue;
                results.Add(prefab);
            }
            
            return results.ToArray();
        }
    }
}