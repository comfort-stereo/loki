﻿using System.Linq;
using UnityEditor;
using UnityEngine;
using UnityEngine.Assertions;
using System.Collections.Generic;

#pragma warning disable 67

namespace LokiPlugin {

    public delegate void OnBeginPlacingGameObjectAction(GameObject obj, bool isPrefab);
    public delegate bool OnCanPlaceGameObjectQuery(GameObject obj, Vector3 position, Vector3 rotation, bool isPrefab);
    public delegate void OnGameObjectPlacedAction(GameObject obj, Vector3 position, Vector3 rotation, bool isPrefab);
    public delegate void OnResetAction();
    public delegate bool HomogeneousComparison(GameObject a, GameObject b);

    public partial class Loki {

        struct MovedGameObjectEntry {
            public GameObject GameObject;
            public Vector3 InitialPosition;
            public Vector3 InitialRotation;
            public Vector3 PositionOffsetFromAnchorObject;
        }
        
        public const string UserDataPath = "Assets/Plugins/Loki/User Data.asset";
        public const string DefaultBasePrefabDirectory = "Assets";
        
        const float SamePositionFudgeFactor = 0.05f;
        const int PlacedObjectsToIgnoreOnAutomaticWindowClose = 2;

        static Loki Self;
        
        WindowController windowController = WindowController.Blank; 
        LokiHUD hud = new LokiHUD();

        HomogeneousComparison homogeneousComparison = DefaultHomogeneousComparision; 

        Tool lastToolUsed;
        
        UserData data;

        bool unityGridWasShown;
        bool isReplacingPrefabInstances;
        int objectPlacementsToIgnore;

        GameObject placementPrefab;
        List<GameObject> selectedPrefabs = new List<GameObject>();
        List<MovedGameObjectEntry> movedGameObjectEntries = new List<MovedGameObjectEntry>(); 

        /// <summary>
        /// Event triggered when a game object is grabbed or you start to place a prefab into the scene.
        /// </summary>
        public event OnBeginPlacingGameObjectAction OnBeginPlacingGameObject;
        
        /// <summary>
        /// Event triggered just before a game object is placed somewhere in the scene. If a subscribed callback returns false,
        /// the placement will be aborted.
        /// </summary>
        public event OnCanPlaceGameObjectQuery OnCanPlaceGameObject; 
        
        /// <summary>
        /// Event triggered after a game object is placed somewhere in the scene.  
        /// </summary>
        public event OnGameObjectPlacedAction OnGameObjectPlaced;
        
        /// <summary>
        /// Event triggered after Loki's Reset() function is called.
        /// </summary>
        public event OnResetAction OnReset;

        /// <summary>
        /// Run from startup initializer.
        /// </summary>
        internal static void OnStartup() {
            Self = Self ?? new Loki();
        }
        
        /// <summary>
        /// Create a new instance of Loki. Load user data and set up callbacks.
        /// </summary>
        Loki() {
            Assert.IsNull(Self, "Created multiple Loki instances.");
            
            /* Load user data. Create it if it doesn't exist. */
            var data = AssetDatabase.LoadAssetAtPath<UserData>(UserDataPath);
            if (data == null) {
                data = ScriptableObject.CreateInstance<UserData>();
                AssetDatabase.CreateAsset(data, UserDataPath);
            }
            this.data = data;

            /* Set up callbacks. */
            SceneView.onSceneGUIDelegate += OnSceneGUI;
            EditorApplication.hierarchyWindowChanged += Refresh;
            AssetDatabaseListener.OnAssetsModified += Refresh;
        }

        /// <summary>
        /// Add all prefabs or instances of prefabs selected by the user to the active palette.
        /// </summary>
        public void AddSelectedPrefabsToPalette() {
            var loki = Loki.Instance;
            var objects = Selection.gameObjects;
            foreach (var obj in objects) {
                if (Ut.IsPrefab(obj)) {
                    loki.SelectedPalette.AddPrefabGUID(Ut.GetGUID(obj));
                } else if (Ut.IsPrefabInstance(obj)) {
                    loki.SelectedPalette.AddPrefabGUID(Ut.GetGUID(PrefabUtility.GetPrefabParent(obj)));
                }
            }
            this.WindowController.Open();
        }
        
        /// <summary>
        /// Default function for HomogeneousComparison. 
        /// </summary>
        /// <param name="prefab">The prefab being placed.</param>
        /// <param name="obj">A game object in the scene.</param>
        /// <returns>True if the prefab and the game object are on the same layer.</returns>
        public static bool DefaultHomogeneousComparision(GameObject prefab, GameObject obj) {
            return prefab.layer == obj.layer;
        }
        
        /// <summary>
        /// Static command to call AddSelectedPrefabsToPalette().
        /// </summary>
        [MenuItem("Assets/Loki/Add Prefab(s) To Palette", false, 0)]
        [MenuItem("GameObject/Loki/Add Prefab(s) To Palette", false, 0)]
        static void AddSelectedPrefabsToPaletteCommand() {
            Instance.AddSelectedPrefabsToPalette();
        }
        
        /// <summary>
        /// Validates the menu item for AddPrefabsToSelectedPalette().
        /// </summary>
        /// <returns>True if any of the selected objects are prefabs or instances of prefabs.</returns>
        [MenuItem("Assets/Loki/Add Prefab(s) To Palette", true, 0)]
        [MenuItem("GameObject/Loki/Add Prefab(s) To Palette", true, 0)] // TODO: Not being run!
        static bool CanAddSelectedPrefabsToPalette() {
            return Selection.gameObjects.Any(Ut.IsPrefabOrInstance);
        }

        /// <summary>
        /// The window controller though which a the main window is manipulated.
        /// </summary>
        internal WindowController WindowController {
            get { return this.windowController; }
            set { this.windowController = value ?? this.windowController; }
        }

        /// <summary>
        /// Get an instance of the Loki singleton. Use this to interact with Loki.
        /// </summary>
        public static Loki Instance {
            get { return Loki.Self ?? (Loki.Self = new Loki()); }
        }

        /// <summary>
        /// The color used to highlight various pieces of the UI.
        /// </summary>
        public Color HighlightColor {
            get { return this.data.HighlightColor; }
            set { this.data.HighlightColor = value; }
        }
        
        /// <summary>
        /// The color used to draw the grid. 
        /// </summary>
        public Color GridColor {
            get { return this.data.GridColor; }
            set { this.data.GridColor = value; }
        }
        
        /// <summary>
        /// The color used to draw grid placement overlaps. 
        /// </summary>
        public Color GridPlacementOverlapColor {
            get { return this.data.GridPlacementOverlapColor; }
            set { this.data.GridPlacementOverlapColor = value; }
        }

        /// <summary>
        /// The key modifiers that must be pressed to rotate a grabbed object.
        /// </summary>
        public EventModifiers KeybindingModifiersRotate {
            get { return this.data.KeybindingModifiersRotate; }
            set { this.data.KeybindingModifiersRotate = value; }
        }
        
        /// <summary>
        /// The key modifiers that must be pressed to change the brush scale with the mouse wheel. 
        /// </summary>
        public EventModifiers KeybindingModifiersChangeBrushScale {
            get { return this.data.KeybindingModifiersChangeBrushScale; }
            set { this.data.KeybindingModifiersChangeBrushScale = value; }
        }
        
        /// <summary>
        /// The keybinding used to trigger a reset. 
        /// </summary>
        public LokiKeybinding KeybindingReset {
            get { return this.data.KeybindingReset; }
            set { this.data.KeybindingReset = value; }
        }
        
        /// <summary>
        /// The keybinding used to grab an object in the scene view and move it around. 
        /// </summary>
        public LokiKeybinding KeybindingGrabGameObjects {
            get { return this.data.KeybindingGrabGameObjects; }
            set { this.data.KeybindingGrabGameObjects = value; }
        }
        
        /// <summary>
        /// The keybinding used to grab an object in the scene view and move it around. 
        /// </summary>
        public LokiKeybinding KeybindingDuplicateAndGrabGameObjects {
            get { return this.data.KeybindingDuplicateAndGrabGameObjects; }
            set { this.data.KeybindingDuplicateAndGrabGameObjects = value; }
        }
        
        /// <summary>
        /// The keybinding used to toggle grid locking on or off. 
        /// </summary>
        public LokiKeybinding KeybindingToggleGridLock {
            get { return this.data.KeybindingToggleGridLock; }
            set { this.data.KeybindingToggleGridLock = value; }
        }
        
        /// <summary>
        /// The keybinding used to toggle the main window on or off. 
        /// </summary>
        public LokiKeybinding KeybindingToggleWindow {
            get { return this.data.KeybindingToggleWindow; }
            set { this.data.KeybindingToggleWindow = value; }
        }
        
        /// <summary>
        /// The keybinding used to select the grid size one up from the current grid size. 
        /// </summary>
        public LokiKeybinding KeybindingSelectGridSizeUp {
            get { return this.data.KeybindingSelectGridSizeUp; }
            set { this.data.KeybindingSelectGridSizeUp = value; }
        }
        
        /// <summary>
        /// The keybinding used to select the grid size one down from the current grid size. 
        /// </summary>
        public LokiKeybinding KeybindingSelectGridSizeDown {
            get { return this.data.KeybindingSelectGridSizeDown; }
            set { this.data.KeybindingSelectGridSizeDown = value; }
        }
        
        /// <summary>
        /// The prefabs selected in the palette view.
        /// </summary>
        public GameObject[] SelectedPrefabs {
            get { return this.selectedPrefabs.ToArray(); }
        } 
        
        /// <summary>
        /// The prefab actively being placed by Loki. Null when no prefab is being placed.
        /// </summary>
        public GameObject PlacementPrefab {
            get { return this.placementPrefab; }
        }

        /// <summary>
        /// Returns the game object used as a preview when placing a prefab.
        /// </summary>
        public GameObject PlacementPrefabPreview {
            get {
                if (this.placementPrefab != null) {
                    return this.MovedGameObjectEntries.FirstOrDefault().GameObject;
                }
                return null;
            }
        }

        /// <summary>
        /// A callback that returns true if a placed prefab should overwrite an object placed at the same grid location. By default, the 
        /// object will be considered homogeneous with the prefab if the object is an instance of the prefab. Set to null to revert to the
        /// default comparison function.
        /// </summary>
        public HomogeneousComparison HomogeneousComparison {
            get { return this.homogeneousComparison; }
            set { this.homogeneousComparison = value ?? DefaultHomogeneousComparision; }
        }
        
        /// <summary>
        /// Specifies whether or not the user is currently replacing prefab instances.
        /// </summary>
        public bool IsReplacingPrefabInstances {
            get { return this.isReplacingPrefabInstances; }
        }

        /// <summary>
        /// Specifies whether Loki's grid should be shown even when not placing an object. False by default.
        /// </summary>
        public bool AlwaysShowGrid {
            get { return this.data.AlwaysShowGrid; }
            set { this.data.AlwaysShowGrid = value; }
        }
        
        /// <summary>
        /// Specifies whether Unity's grid should be hidden when Loki's grid is shown. True by default.
        /// </summary>
        public bool HideUnityGridWhenGridIsShown {
            get { return this.data.HideUnityGridWhenGridIsShown; }
            set { this.data.HideUnityGridWhenGridIsShown = value; }
        }
        
        /// <summary>
        /// Specifies whether Unity's grid should be hidden when Loki's grid is shown. True by default.
        /// </summary>
        public bool CloseWindowWhenPlacingPrefab {
            get { return this.data.CloseWindowWhenPlacingPrefab; }
            set { this.data.CloseWindowWhenPlacingPrefab = value; }
        }

        /// <summary>
        /// Specifies whether objects should be locked to the grid when moved.
        /// </summary>
        public bool GridLockEnabled {
            get { return this.data.GridLockEnabled; }
            set { this.data.GridLockEnabled = value; }
        }

        /// <summary>
        /// The type of grid locking to use when moving objects.
        /// </summary>
        public GridLockMode GridLockMode {
            get { return this.data.GridLockMode; }
            set { this.data.GridLockMode = value; }
        }

        /// <summary>
        /// The number of degrees a moved object is rotated per click of the mouse wheel.
        /// </summary>
        public float RotationDelta {
            get { return this.data.RotationDelta; }
            set { this.data.RotationDelta = value; }
        }
        
        /// <summary>
        /// Returns a shallow copied array of the available grid sizes.
        /// </summary>
        public float[] GridSizes {
            get { return this.data.GridSizes.ToArray(); }
        }
        
        /// <summary>
        /// The index of the grid size in GridSizes that is currently selected.
        /// </summary>
        public int SelectedGridSizeIndex {
            get { return this.data.SelectedGridSizeIndex; }
            set { this.data.SelectedGridSizeIndex = value.InBoundsOf(this.data.GridSizes); }
        }
        
        /// <summary>
        /// The grid size currently selected as determined by SelectedGridSizeIndex.
        /// </summary>
        public float SelectedGridSize {
            get { return this.data.GridSizes[this.data.SelectedGridSizeIndex]; }
        }
        
        /// <summary>
        /// A shallow copied array of all available palettes.
        /// </summary>
        internal LokiPalette[] Palettes {
            get { return this.data.Palettes.ToArray(); }
        }
        
        /// <summary>
        /// The index of the selected palette in Palettes.
        /// </summary>
        public int SelectedPaletteIndex {
            get { return this.data.SelectedPaletteIndex; }
            set { this.data.SelectedPaletteIndex = value.InBoundsOf(this.data.Palettes); }
        }

        /// <summary>
        /// The palette Loki currently has selected as determined by SelectedPaletteIndex.
        /// </summary>
        public LokiPalette SelectedPalette {
            get { return this.data.Palettes[this.data.SelectedPaletteIndex]; }
        }

        /// <summary>
        /// The directory in which Loki will look for prefabs. Defaults to "Assets".
        /// </summary>
        public string BasePrefabDirectory {
            get { return this.data.BasePrefabDirectory; }
            set {
                if (this.data.BasePrefabDirectory != value) {
                    this.data.BasePrefabDirectory = value;
                    Refresh();
                }
            }
        }

        /// <summary>
        /// Directories in the base prefab directory that Loki will ignore when searching for prefabs. Includes "Editor Default Resources" 
        /// by default.
        /// </summary>
        public string[] IgnoredPrefabDirectories {
            get { return this.data.IgnoredPrefabDirectories.ToArray(); }
            set { this.data.IgnoredPrefabDirectories = value.ToList(); }
        }
        
        /// <summary>
        /// References to game objects being moved in the scene. Ensures that no referenced game objects have been destroyed.
        /// </summary>
        List<MovedGameObjectEntry> MovedGameObjectEntries {
            get {
                this.movedGameObjectEntries.RemoveAll(entry => entry.GameObject == null);
                return this.movedGameObjectEntries;
            }
        }

        /// <summary>
        /// Add a directory to the ignored prefab directories if it is not already present. 
        /// </summary>
        /// <param name="directory"></param>
        /// <returns>True if the directory was added.</returns>
        public bool AddIgnoredPrefabDirectory(string directory) {
            var added = !this.data.IgnoredPrefabDirectories.Contains(directory);
            if (added) {
                this.data.IgnoredPrefabDirectories.Add(directory);
                Refresh();
            }
            return added;
        }

        /// <summary>
        /// Remove a directory from the ignored prefab directories if it is present.
        /// </summary>
        /// <param name="directory"></param>
        /// <returns>True if the directory was removed.</returns>
        public bool RemoveIgnoredPrefabDirectory(string directory) {
            var removed = this.data.IgnoredPrefabDirectories.Remove(directory);
            if (removed) {
                Refresh();
            }
            return removed;
        }
        
        /// <summary>
        /// Stop moving objects or placing a selected prefab. 
        /// </summary>
        /// <param name="resetMovedObjectPositions">
        /// Specifies whether moved objects should be returned to their previous positions after being moved. 
        /// </param> 
        public void Reset(bool resetMovedObjectPositions) {
            Tools.current = this.lastToolUsed;

            this.WindowController.Refresh();
            
            if (this.WindowController.IsOpen) {
                Event.current.Use();
            }
            
            if (resetMovedObjectPositions) {
                var moved = this.MovedGameObjectEntries; 
                foreach (var entry in moved) {
                    entry.GameObject.transform.position = entry.InitialPosition;
                    entry.GameObject.transform.eulerAngles = entry.InitialRotation;
                }
            }

            DeselectAllPrefabs();
            this.MovedGameObjectEntries.Clear(); 
            this.isReplacingPrefabInstances = false;
            if (this.OnReset != null) {
                this.OnReset();
            }
        }

        /// <summary>
        /// Toggles grid locking of moved objects on or off.
        /// </summary>
        public void ToggleGridLock() {
            this.GridLockEnabled = !this.GridLockEnabled;
        }
        
        /// <summary>
        /// Checks if a prefab is selected in the palette. 
        /// </summary>
        /// <param name="prefab"></param>
        /// <returns></returns>
        public bool HasPrefabSelected(GameObject prefab) {
            return this.selectedPrefabs.Contains(prefab);
        }

        /// <summary>
        /// Select a single prefab.
        /// </summary>
        /// <param name="prefab">The prefab to select.</param>
        public void SelectPrefab(GameObject prefab) {
            this.selectedPrefabs.Clear();
            this.selectedPrefabs.Add(prefab);
        }

        /// <summary>
        /// Add a prefab to the selected prefabs if it is not already selected.
        /// </summary>
        /// <param name="prefab">The prefab to add to the selected prefab.</param>
        public void SelectPrefabAdditive(GameObject prefab) {
            if (HasPrefabSelected(prefab)) return;
            this.selectedPrefabs.Add(prefab); 
        }

        /// <summary>
        /// Deselect a prefab if it is selected.
        /// </summary>
        /// <param name="prefab">The prefab to deselect.</param>
        public void DeselectPrefab(GameObject prefab) {
            if (!HasPrefabSelected(prefab)) return;
            if (prefab == this.PlacementPrefab) {
                StopPlacingPrefab();
            }
            this.selectedPrefabs.Remove(prefab);
        }

        /// <summary>
        /// Deselect all currently selected prefabs, stopping prefab placement as a consequence.
        /// </summary>
        public void DeselectAllPrefabs() {
            this.selectedPrefabs.Clear();
            if (this.placementPrefab != null) {
                StopPlacingPrefab();
            }
        }

        /// <summary>
        /// Start placing a prefab into the scene.
        /// </summary>
        /// <param name="prefab">The prefab to start placing.</param>
        public void BeginPlacingPrefab(GameObject prefab) {
            this.unityGridWasShown = UnityGridUtility.ShowUnityGrid;

            if (this.OnBeginPlacingGameObject != null) {
                this.OnBeginPlacingGameObject(prefab, true);
            }
            
            if (this.placementPrefab == prefab) return;
            
            SelectPrefab(prefab);
            
            this.placementPrefab = prefab;
            
            var preview = Ut.Copy(prefab, "Preview");
            
            Selection.activeGameObject = preview;

            this.MovedGameObjectEntries.Clear();
            this.MovedGameObjectEntries.Add(new MovedGameObjectEntry { GameObject = preview });

            if (this.data.CloseWindowWhenPlacingPrefab) {
                this.WindowController.Close();
                this.objectPlacementsToIgnore = PlacedObjectsToIgnoreOnAutomaticWindowClose;
            }
            
            Ut.FocusSceneView();
        }

        /// <summary>
        /// Stop placing a prefab into the scene.
        /// </summary>
        public void StopPlacingPrefab() {
            UnityGridUtility.ShowUnityGrid = this.unityGridWasShown;
            
            this.placementPrefab = null;
            this.selectedPrefabs.Clear();

            var moved = this.MovedGameObjectEntries; 
            if (moved.Count != 0) {
                foreach (var entry in moved) {
                    Object.DestroyImmediate(entry.GameObject);
                }
                moved.Clear();
            }
            
            Selection.activeGameObject = null;
        }

        /// <summary>
        /// Refresh all palette results and current game object references to match the current project state.
        /// </summary>
        public void Refresh() {
            this.selectedPrefabs.RemoveAll(prefab => prefab == null);
            foreach (var palette in this.data.Palettes) {
                palette.Refresh();
            }
            this.WindowController.Refresh();
        }

        /// <summary>
        /// Add a new grid size to select from. Negative grid sizes or grid sizes already available are ignored.
        /// </summary>
        /// <param name="gridSize">
        /// The grid size to add.
        /// </param>
        public void AddGridSize(float gridSize) {
            if (this.data.GridSizes.Contains(gridSize)) return;
            if (gridSize > 0) {
                this.data.GridSizes.Add(gridSize);
                this.data.GridSizes.Sort();
                this.SelectedGridSizeIndex = this.data.GridSizes.IndexOf(gridSize);
            }
        }

        /// <summary>
        /// Delete the grid size Loki currently has selected. Ignored if only one grid size is available.
        /// </summary>
        public void RemoveSelectedGridSize() {
            if (this.data.GridSizes.Count > 1) {
                this.data.GridSizes.RemoveAt(this.SelectedGridSizeIndex);
                this.data.SelectedGridSizeIndex = this.data.SelectedGridSizeIndex.InBoundsOf(this.data.GridSizes);
            }
        }

        /// <summary>
        /// Add a new, blank palette to Loki and return it.
        /// </summary>
        /// <returns>The added palette.</returns>
        public LokiPalette AddPalette() {
            var palette = new LokiPalette();
            this.data.Palettes.Add(palette);
            this.data.SelectedPaletteIndex = this.data.Palettes.IndexOf(palette);
            return palette;
        }

        /// <summary>
        /// Delete the selected palette from the palette list.
        /// </summary>
        public void RemoveSelectedPalette() {
            if (this.data.Palettes.Count > 1) {
                this.data.Palettes.RemoveAt(this.data.SelectedPaletteIndex);
                this.data.SelectedPaletteIndex = this.data.SelectedPaletteIndex.InBoundsOf(this.data.Palettes);
            }
        }
        
        /// <summary>
        /// Validates menu items for RemoveSelectedPrefabsFromPalette(). 
        /// </summary>
        /// <returns>True if any selected prefabs in the active palette were directly added.</returns>
        internal bool CanRemoveSelectedPrefabsFromPalette() {
            var guids = this.SelectedPalette.AddedPrefabGUIDs;
            return this.SelectedPrefabs.Any(prefab => guids.Contains(Ut.GetGUID(prefab)));  
        }

        /// <summary>
        /// Remove selected prefabs from the active palette provided they were added directly.
        /// </summary>
        public void RemoveSelectedPrefabsFromPalette() {
            foreach (var prefab in this.SelectedPrefabs) {
                this.SelectedPalette.RemovePrefabGUID(Ut.GetGUID(prefab));
            }
            DeselectAllPrefabs();
            this.WindowController.Refresh();
        }
        
        /// <summary>
        /// Validates menu items for AddSelectedPrefabsToPaletteDirectly(). 
        /// </summary>
        /// <returns>True if any selected prefabs in the active palette were directly added already.</returns>
        internal bool CanAddSelectedPrefabsToPaletteDirectly() {
            var guids = this.SelectedPalette.AddedPrefabGUIDs;
            return this.SelectedPrefabs.Any(prefab => !guids.Contains(Ut.GetGUID(prefab)));  
        }

        /// <summary>
        /// Add selected prefabs in the active palette directly to the palette so they no longer depend on any search.
        /// </summary>
        public void AddSelectedPrefabsToPaletteDirectly() {
            foreach (var prefab in this.SelectedPrefabs) {
                AddPrefabGUIDToPaletteDirectly(Ut.GetGUID(prefab));
            }
        }

        /// <summary>
        /// Add a prefab to the active palette by its guid. 
        /// </summary>
        /// <param name="guid">The guid of the prefab to add to the active palette directly.</param>
        internal void AddPrefabGUIDToPaletteDirectly(string guid) {
            this.SelectedPalette.AddPrefabGUID(guid);
        }

        /// <summary>
        /// Reveal a selected prefab's location in the project.
        /// </summary>
        public void RevealSelectedPrefabLocation() {
            var prefab = this.selectedPrefabs.FirstOrDefault();
            if (prefab != null) {
                Selection.activeGameObject = prefab;
                EditorGUIUtility.PingObject(prefab);
            }
        }

        /// <summary>
        /// Returns all game objects in the editor's scene hierarchy that are instances of selected prefabs.
        /// </summary>
        /// <returns>An array of prefab instances.</returns>
        public GameObject[] GetAllInstancesOfSelectedPrefabs() {
            var prefabs = this.SelectedPrefabs;
            var moved = this.MovedGameObjectEntries;
            var instances = Ut.FindGameObjectsWhere(obj => {
                if (moved.Any(entry => entry.GameObject == obj)) return false;
                var prefab = PrefabUtility.GetPrefabParent(obj);
                return prefab != null && prefabs.Contains(prefab);
            });
            return instances;
        }

        /// <summary>
        /// Checks if any selected prefabs have an instance in the scene.
        /// </summary>
        /// <returns>True if any selected prefabs have an instance.</returns>
        internal bool SelectedPrefabsHaveInstance() {
            var prefabs = this.SelectedPrefabs;
            var moved = this.MovedGameObjectEntries;
            var exists = Ut.GameObjectExistsWhere(obj => {
                if (moved.Any(entry => entry.GameObject == obj)) return false;
                var prefab = PrefabUtility.GetPrefabParent(obj);
                return prefab != null && prefabs.Contains(prefab);
            });
            return exists;
        }

        /// <summary>
        /// Reverts all instances of selected prefabs in the scene.
        /// </summary>
        public void RevertInstancesOfSelectedPrefabs() {
            var instances = GetAllInstancesOfSelectedPrefabs();
            var count = 0;
            foreach (var instance in instances) {
                EditorUtility.DisplayProgressBar("Reverting Instances", "", ((float) instances.Length) / ++count);
                Undo.RegisterFullObjectHierarchyUndo(instance, "Revert To Prefab");
                PrefabUtility.RevertPrefabInstance(instance);
            }
            EditorUtility.ClearProgressBar();
        }

        /// <summary>
        /// Select all game objects in the editor's scene hierarchy that are instances of any selected prefabs.
        /// </summary>
        public void SelectInstancesOfSelectedPrefabs() {
            var instances = GetAllInstancesOfSelectedPrefabs();
            Selection.objects = instances.Select(obj => (Object) obj).ToArray();
            
            if (SceneView.lastActiveSceneView != null) {
                SceneView.lastActiveSceneView.Focus();
            }
        }

        /// <summary>
        /// Delete all game objects in the editor's scene hierarchy that are instances of any selected prefabs. Can use undo to reverse this.
        /// </summary>
        internal void DeleteInstancesOfSelectedPrefabs() {
            var instances = GetAllInstancesOfSelectedPrefabs();
            var count = 0;
            foreach (var obj in instances) {
                EditorUtility.DisplayProgressBar("Deleting Instances", "", ((float) instances.Length) / ++count);
                Undo.DestroyObjectImmediate(obj);
            }
            if (SceneView.lastActiveSceneView != null) {
                SceneView.lastActiveSceneView.Focus();
            }
            EditorUtility.ClearProgressBar();
        }

        /// <summary>
        /// Begin replacing all instances of selected prefabs in the scene with another prefab. Wait until another palette entry is clicked.
        /// </summary>
        internal void BeginReplaceInstancesOfSelectedPrefabs() {
            this.isReplacingPrefabInstances = true;
        }

        /// <summary>
        /// Replace all instances of selected prefabs in the scene with another prefab.
        /// </summary>
        public void ReplaceInstancesOfSelectedPrefabsWith(GameObject replacement) {
            var instances = GetAllInstancesOfSelectedPrefabs();
            var count = 0;
            foreach (var obj in instances) {
                EditorUtility.DisplayProgressBar("Replacing Instances", "", ((float) instances.Length) / ++count);
                PlaceGameObject(replacement, obj.transform.position, obj.transform.eulerAngles, true);
                
                /* ReSharper disable once ConditionIsAlwaysTrueOrFalse */
                if (obj != null) {
                    Undo.DestroyObjectImmediate(obj);
                }
            }
            EditorUtility.ClearProgressBar();
            this.isReplacingPrefabInstances = false;
        }

        /// <summary>
        /// Replace all selected game objects in the scene with a selected prefab.
        /// </summary>
        public void ReplaceSelectedGameObjectsWithSelectedPrefab() {
            
            var prefab = this.SelectedPrefabs.FirstOrDefault();
            if (prefab == null) return;
            
            var objects = Selection.gameObjects;
            var count = 0;
            foreach (var obj in objects) {
                EditorUtility.DisplayProgressBar("Replacing Instances", "", ((float) objects.Length) / ++count);
                PlaceGameObject(prefab, obj.transform.position, obj.transform.eulerAngles, true);
                
                /* ReSharper disable once ConditionIsAlwaysTrueOrFalse */
                if (obj != null) {
                    Undo.DestroyObjectImmediate(obj);
                }
            }
            EditorUtility.ClearProgressBar();
        }

        /// <summary>
        /// Validates ReplaceSelectedGameObjectsWithSelectedPrefab().
        /// </summary>
        /// <returns>True if all selected game objects are in the scene.</returns>
        internal bool CanReplaceSelectedGameObjectsWithSelectedPrefab() {
            return Selection.activeGameObject != null && Selection.gameObjects.All(obj => !Ut.IsPrefab(obj));
        }

        /// <summary>
        /// Duplicate all selected game objects and grab them.
        /// </summary>
        public void DuplicateAndGrabSelectedGameObjects() {
            var copies = Selection.gameObjects.Select(obj => (Object) Ut.Copy(obj)).ToArray();
            foreach (var copy in copies) {
                Undo.RegisterCreatedObjectUndo(copy, "Copied Object");
            }
            Selection.objects = copies;
            GrabSelectedGameObjects();
        }

        /// <summary>
        /// Open menu prompt asking to delete the selected prefabs asset files.
        /// </summary>
        public void PromptToDeleteSelectedPrefabs() {
            var selected = this.SelectedPrefabs;
            var names = string.Join("\n", selected.Select(prefab => prefab.name).ToArray());
            var question = "Permanently Delete " + selected.Length + " Prefab(s)?:\n\n" + names;
            if (EditorUtility.DisplayDialog("Loki", question, "Yes", "No")) {
                var count = 0;
                foreach (var prefab in selected) {
                    EditorUtility.DisplayProgressBar("Deleting Prefabs", "", ((float) names.Length) / ++count);
                    var path = AssetDatabase.GetAssetPath(prefab);
                    if (path != null) {
                        AssetDatabase.DeleteAsset(path);
                    }
                }
            }
            EditorUtility.ClearProgressBar();
            this.WindowController.Focus();
        } 
        
        /// <summary>
        /// Called externally by the main window. 
        /// </summary>
        internal void GUIUpdate() {
            SortPalettesByLabel();
            SortIgnoredPrefabDirectories();
            HandleKeybindings(Event.current);
        }
        
        /// <summary>
        /// Sort all palette's alphabetically by their labels.
        /// </summary>
        void SortPalettesByLabel() {
            var selected = this.SelectedPalette;
            this.data.Palettes.Sort((a, b) => string.CompareOrdinal(a.Label, b.Label));
            this.data.SelectedPaletteIndex = this.data.Palettes.IndexOf(selected);
        }

        /// <summary>
        /// Sort ignored prefab directories alphabetically.
        /// </summary>
        void SortIgnoredPrefabDirectories() {
            this.data.IgnoredPrefabDirectories.Sort();
        }
        
        /// <summary>
        /// Enables or disables the grid based on Loki's current state.
        /// </summary>
        void ControlGridVisiblity() {
            GizmoDrawer.Instance.HideGrid();
            if (this.data.AlwaysShowGrid || this.placementPrefab != null) {
                if (this.data.HideUnityGridWhenGridIsShown) {
                    UnityGridUtility.ShowUnityGrid = false;
                }
                GizmoDrawer.Instance.ShowGrid(this.data.GridColor, this.SelectedGridSize);
            } 
        }

        /// <summary>
        /// Checks to see if a keybinding is triggered by an input event. Returns false if the keybinding is null.
        /// </summary>
        /// <param name="keybinding">The keybinding to check. Can be null</param>
        /// <param name="input">The input event to evaluate.</param>
        /// <returns>True if the keybinding is triggered by the event.</returns>
        bool KeybindingTriggered(LokiKeybinding keybinding, Event input) {
            if (keybinding == null || !keybinding.Matches(input)) {
                return false;
            }
            if (keybinding.RequiresSceneFocus) {
                if (EditorWindow.focusedWindow != SceneView.currentDrawingSceneView) {
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// Check for keybinding presses in the input event and run their associated commands.
        /// </summary>
        void HandleKeybindings(Event input) {
            var use = false;

            if (input.type == EventType.KeyDown && input.keyCode == KeyCode.Delete) {
                if (this.windowController.IsFocused) {
                    RemoveSelectedPrefabsFromPalette();
                } else {
                    Reset(false);
                }
            } else if (KeybindingTriggered(this.data.KeybindingReset, input)) {
                Reset(true);
            } else if (KeybindingTriggered(this.data.KeybindingGrabGameObjects, input)) {
                GrabSelectedGameObjects();
                use = true;
            } else if (KeybindingTriggered(this.data.KeybindingDuplicateAndGrabGameObjects, input)) {
                DuplicateAndGrabSelectedGameObjects();
                use = true;
            } else if (KeybindingTriggered(this.data.KeybindingToggleGridLock, input)) {
                ToggleGridLock();
                use = true;
            } else if (KeybindingTriggered(this.data.KeybindingToggleWindow, input)) {
                this.WindowController.Toggle();
                use = true;
            } else if (KeybindingTriggered(this.data.KeybindingSelectGridSizeUp, input)) {
                this.SelectedGridSizeIndex = (this.SelectedGridSizeIndex + 1).InBoundsOf(this.data.GridSizes);
            } else if (KeybindingTriggered(this.data.KeybindingSelectGridSizeDown, input)) {
                this.SelectedGridSizeIndex = (this.SelectedGridSizeIndex - 1).InBoundsOf(this.data.GridSizes);
            }

            if (use) {
                input.Use();
            }
        }
        
        /// <summary>
        /// Draw to the scene and handle keybindings.
        /// </summary>
        /// <param name="view">The scene view to draw to.</param>
        void OnSceneGUI(SceneView view) {
            this.hud.Draw(view, new [] {
                this.data.GridLockEnabled ? "Grid Lock On" : "Grid Lock Disabled",
                "Grid Lock Mode: " + this.data.GridLockMode,
                "Grid Size: " + this.SelectedGridSize
            });
            
            ControlGridVisiblity();
            
            var input = Event.current;
            if (input == null) return;

            HandleKeybindings(input);
            HandleObjectPlacement(input, view);    
        }
    }
}
