using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using System.Linq;

namespace LokiPlugin {
    public class LokiHUD {

        readonly Color BackgroundColor = new Color(0.5f, 0.5f, 0.5f, 0.5f);
        
        const int Width = 200;
        const int Height = 200;
        const int MaxLogCount = 100;

        Queue<string> log = new Queue<string>();

        Vector2 logScroll;

        bool showFullLog;
        bool keepLogScrollAtBottom;

        public LokiHUD() {
            LoadPreferences();
        }
        
        /// <summary>
        /// Log a message to show on the screen.
        /// </summary>
        /// <param name="message">The message to show.</param>
        public void Log(string message) {
            this.log.Enqueue(message);
            if (this.log.Count > MaxLogCount) {
                this.log.Dequeue();
            }
            if (this.keepLogScrollAtBottom) {
                this.logScroll.y = Height;
            }
        }

        /// <summary>
        /// Clear all logged messages. 
        /// </summary>
        public void ClearLog() {
            this.log.Clear();
        }
        
        /// <summary>
        /// Draw the HUD to a scene view. Display information about Loki's current state. 
        /// </summary>
        /// <param name="view">The scene view to draw the HUD to.</param>
        /// <param name="content">The information to display.</param>
        internal void Draw(SceneView view, string[] content) {
            var screen = view.camera.pixelRect;
            var area = new Rect(screen.width - Width, 5f, Width, Height);
            
            Handles.BeginGUI();
            
            var style = new GUIStyle(EditorStyles.helpBox);
            var previousColor = GUI.backgroundColor;
            
            GUI.backgroundColor = this.BackgroundColor;
            
            GUILayout.BeginArea(area);
            EditorGUILayout.BeginVertical(style);
            {
                GUILayout.Label("Loki", Style.LabelCenter);
                foreach (var line in content) {
                    GUILayout.Label(line);
                }
                DrawLog();
            }
            EditorGUILayout.EndVertical();
            GUILayout.EndArea();

            GUI.backgroundColor = previousColor;
            
            Handles.EndGUI();
            
            SavePreferences();
        }

        void LoadPreferences() {
            this.showFullLog = EditorPrefs.GetBool("Loki HUD Show Full Log");
            this.keepLogScrollAtBottom = EditorPrefs.GetBool("Loki HUD Keep Log Scroll At Bottom");
        }

        void SavePreferences() {
            EditorPrefs.SetBool("Loki HUD Show Full Log", this.showFullLog);
            EditorPrefs.SetBool("Loki HUD Keep Log Scroll At Bottom", this.keepLogScrollAtBottom);
        }

        void DrawLog() {
            
            this.showFullLog = EditorGUILayout.Foldout(this.showFullLog, "Log");
            
            if (this.showFullLog) {
                this.logScroll = EditorGUILayout.BeginScrollView(this.logScroll, Style.Enclosed);
                {
                    foreach (var line in this.log) {
                        GUILayout.TextArea(line);
                    }
                }
                EditorGUILayout.EndScrollView();
                this.keepLogScrollAtBottom = EditorGUILayout.ToggleLeft("Keep At Bottom", this.keepLogScrollAtBottom);
            } else {
                EditorGUILayout.BeginVertical(Style.Enclosed);
                {
                    if (this.log.Count != 0) {
                        GUILayout.TextArea(this.log.Last());
                    }
                }
                EditorGUILayout.EndVertical();
            }
        }
    }
}