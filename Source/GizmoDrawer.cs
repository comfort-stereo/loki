﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;

namespace LokiPlugin {
    class GizmoDrawer : MonoBehaviour {

        struct GridSpaceIndicatorEntry {
            public Vector3 Position;
            public Color Color;
            public float Size;
        }
        
        const float TotalGridScale = 2000f;
        
        static GizmoDrawer Self;

        bool showGrid;
        GameObject grid;
        GameObject gridSpaceIndicator;
        Color gridColor;
        float gridSize;
        GridLockMode gridLockMode;
        List<GridSpaceIndicatorEntry> gridSpaceIndicatorEntries = new List<GridSpaceIndicatorEntry>();
        
        /// <summary>
        /// Returns an instance of the gizmo drawer singleton.
        /// </summary>
        public static GizmoDrawer Instance {
            get {
                if (Self == null) {
                    var other = Resources.FindObjectsOfTypeAll<GizmoDrawer>();
                    foreach (var drawer in other) {
                        Object.DestroyImmediate(drawer);
                    }

                    var hider = new GameObject("Gizmo Drawer Hider");
                    
                    Self = new GameObject("Gizmo Drawer", typeof(GizmoDrawer)).GetComponent<GizmoDrawer>();
                    Self.transform.parent = hider.transform;
                    
                    hider.hideFlags = HideFlags.HideAndDontSave | HideFlags.NotEditable;
                    hider.transform.position = new Vector3(float.MaxValue, float.MaxValue, float.MaxValue);
                    
                    Self.gameObject.hideFlags = HideFlags.DontSave | HideFlags.NotEditable | HideFlags.HideInInspector;
                    Self.transform.position = hider.transform.position; 
                }

                return Self;
            }
        }
        
        /// <summary>
        /// Run on startup.
        /// </summary>
        public static void OnStartup() { }

        /// <summary>
        /// Draw the grid and all grid space indicators into the scene if specified.
        /// </summary>
        void OnDrawGizmos() {
            if (this.showGrid) {
                DrawGrid(this.gridColor, this.gridSize);
            }
            foreach (var entry in this.gridSpaceIndicatorEntries) {
                DrawGridSpaceIndicator(entry.Position, entry.Color, entry.Size);
            }
        }
        
        /// <summary>
        /// Shows the grid the next time gizmos are drawn.
        /// </summary>
        /// <param name="color">The color to draw the grid with.</param>
        /// <param name="size">The size of each grid cell drawn.</param>
        public void ShowGrid(Color color, float size) {
            this.showGrid = true;
            this.gridSize = size;
            this.gridColor = color;
        }
        
        /// <summary>
        /// Hide the grid the next time gizmos are drawn.
        /// </summary>
        public void HideGrid() {
            this.showGrid = false;
        }
        
        /// <summary>
        /// Draws a grid space indicator into the scene view the next time gizmos are drawn. Remains until indicators are cleared.
        /// </summary>
        /// <param name="position">The position of the indicator.</param>
        /// <param name="color">The color to draw the indicator with.</param>
        /// <param name="size">The size of the indicator's box.</param>
        public void ShowGridSpaceIndicator(Vector3 position, Color color, float size) {
            this.gridSpaceIndicatorEntries.Add(new GridSpaceIndicatorEntry { Position = position, Color = color, Size = size });
        }

        /// <summary>
        /// Clear all drawn grid space indicators from the scene view.
        /// </summary>
        public void ClearGridSpaceIndicators() { 
            this.gridSpaceIndicatorEntries.Clear();
        }

        /// <summary>
        /// Immediately draw the grid to the scene.
        /// </summary>
        /// <param name="color">The color to draw the grid with.</param>
        /// <param name="size">The size of each grid cell.</param>
        void DrawGrid(Color color, float size) {
            if (this.grid == null) {
                this.grid = EditorGUIUtility.Load("Loki/Grid.prefab") as GameObject;
                if (this.grid == null) {
                    Debug.LogError("Grid.prefab could not be found in Loki's editor default resources.");
                    return;
                }
            }

            var view = SceneView.currentDrawingSceneView;
            var camera = view.camera;
            var mesh = this.grid.GetComponent<MeshFilter>().sharedMesh;
            var renderer = this.grid.GetComponent<MeshRenderer>();
            var material = renderer.sharedMaterial;
            var scale = Vector3.one * TotalGridScale;
            var offset = new Vector3(size / 2f, size / 2f);
            
            var position = GridUtility.MovePositionToGrid(
                new Vector3(camera.transform.position.x, camera.transform.position.y, 0f), GridLockMode.Center, size
            ) + offset;

            var textureScale = scale / size;
            var textureOffset = new Vector3(1f - (textureScale.x % 1f) / 2f, 1f - (textureScale.y % 1f) / 2f);

            material.mainTextureScale = textureScale;
            material.mainTextureOffset = textureOffset;
            material.SetColor("_TintColor", color);
            
            if (mesh != null && material.SetPass(0)) {
                Graphics.DrawMeshNow(mesh, Matrix4x4.TRS(position, Quaternion.identity, scale));
            }
        }
        
        /// <summary>
        /// Immediately draw a grid space indicator to the scene.
        /// </summary>
        /// <param name="position">The position of the indicator.</param>
        /// <param name="color">The color to draw the indicator with.</param>
        /// <param name="size">The size of the indicator's box.</param>
        void DrawGridSpaceIndicator(Vector3 position, Color color, float size) {
            if (this.gridSpaceIndicator == null) {
                this.gridSpaceIndicator = EditorGUIUtility.Load("Loki/Grid Space Indicator.prefab") as GameObject;
                if (this.gridSpaceIndicator == null) {
                    Debug.LogError("Grid Space Indicator.prefab could not be found in Loki's editor default resources.");
                    return;
                }
            }
            
            var mesh = this.gridSpaceIndicator.GetComponent<MeshFilter>().sharedMesh;
            var renderer = this.gridSpaceIndicator.GetComponent<MeshRenderer>();
            var material = renderer.sharedMaterial;
            var scale = Vector3.one * size;

            material.SetColor("_TintColor", color);
            
            if (mesh != null && material.SetPass(0)) {
                Graphics.DrawMeshNow(mesh, Matrix4x4.TRS(position, Quaternion.identity, scale));
            }
        }
    }
}