using UnityEditor;

namespace LokiPlugin {

    delegate void OnAssetsModifiedAction();
    
    class AssetDatabaseListener : AssetPostprocessor {

        internal static event OnAssetsModifiedAction OnAssetsModified;
        
        static void OnPostprocessAllAssets(string[] importedAssets, string[] deletedAssets, string[] movedAssets, string[] movedFromAssetPaths) {
            if (OnAssetsModified != null) {
                OnAssetsModified();
            }
        }
    }
}