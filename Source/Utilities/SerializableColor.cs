using System;
using UnityEngine;

namespace LokiPlugin {
    
    [Serializable]
    class SerializableColor {
        [SerializeField] float R;
        [SerializeField] float G;
        [SerializeField] float B;
        [SerializeField] float A = 1f;

        SerializableColor() { }

        public SerializableColor(Color color) {
            Set(color);
        }

        public Color Color {
            get { return new Color(this.R, this.G, this.B, this.A); }
        }

        public void Set(Color color) {
            this.R = color.r;
            this.G = color.g;
            this.B = color.b;
            this.A = color.a;
        }
    }
}