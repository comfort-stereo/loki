﻿using UnityEngine;
using UnityEditor;
using System.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using System.Reflection;

namespace LokiPlugin {
    
    public delegate void Callback();
    public delegate bool QueryCallback();
    public delegate void Action<in T>(T obj);
    
    static class Ut {
        public static bool IsPrefab(GameObject obj) {
            return PrefabUtility.GetPrefabType(obj) == PrefabType.Prefab;
        }

        public static bool IsPrefabInstance(GameObject obj) {
            return PrefabUtility.GetPrefabType(obj) == PrefabType.PrefabInstance;
        }

        public static bool IsPrefabOrInstance(GameObject obj) {
            return IsPrefab(obj) || IsPrefabInstance(obj);
        }

        public static int Between(this int value, int min, int max) {
            return Mathf.Min(max, Mathf.Max(min, value));
        }

        public static int InBoundsOf<T>(this int value, List<T> list) {
            return Between(value, 0, list.Count - 1);
        }

        public static int InBoundsOf<T>(this int value, T[] array) {
            return Between(value, 0, array.Length - 1);
        }

        public static Type GetType(string name) {
            var targetType = Type.GetType(name);
            if (targetType != null) {
                return targetType;
            }

            foreach (var assembly in AppDomain.CurrentDomain.GetAssemblies()) {
                foreach (var type in assembly.GetTypes()) {
                    if (type.Name == name) {
                        return type;
                    }
                }
            }
            return null;
        }

        public static Type GetComponentType(string name) {
            var targetType = Type.GetType(name);
            if (targetType != null && targetType.IsSubclassOf(typeof(Component))) {
                return targetType;
            }

            foreach (var assembly in AppDomain.CurrentDomain.GetAssemblies()) {
                foreach (var type in assembly.GetTypes()) {
                    if (type.Name == name && type.IsSubclassOf(typeof(Component))) {
                        return type;
                    }
                }
            }
            return null;
        }

        public static bool ComponentExists(string name) {
            return GetComponentType(name) != null;
        }

        public static string GetGUID(UnityEngine.Object obj) {
            return AssetDatabase.AssetPathToGUID(AssetDatabase.GetAssetPath(obj));
        }

        public static UnityEngine.Object GetObjectFromGUID(string guid) {
            return AssetDatabase.LoadAssetAtPath(AssetDatabase.GUIDToAssetPath(guid), typeof(UnityEngine.Object));
        }

        
        public static T Apply<T>(this T obj, Action<T> action) where T : class {
            action(obj);
            return obj; 
        }

        public static GameObject Copy(GameObject obj, string name = null) {
            GameObject copy;
            if (Ut.IsPrefab(obj)) {
                copy = (GameObject) PrefabUtility.InstantiatePrefab(obj);
            } else {
                copy = UnityEngine.Object.Instantiate(obj);
            }

            copy.name = name ?? obj.name;
            
            return copy;
        }

        public static GameObject Copy(GameObject obj, string name, HideFlags hideFlags) {
            var copy = Copy(obj, name);
            copy.hideFlags = hideFlags;
            
            return copy;
        }

        public static bool TagExists(string tag) {
            try {
                GameObject.FindGameObjectWithTag(tag);
            } catch (Exception) {
                return false;
            }
            return true;
        }

        public static bool LayerExists(string layer) {
            var number = LayerMask.NameToLayer(layer);
            return number > -1 && number < 33;
        }

        public static Rect SetWidth(this Rect rect, float width) {
            return new Rect(
                rect.x,
                rect.y,
                width,
                rect.height
            );
        }
        
        public static Rect SetX(this Rect rect, float x) {
            return new Rect(
                x,
                rect.y,
                rect.width,
                rect.height
            );
        }

        public static string RelativeAssetDirectoryPathDialog() {
            var project = Path.GetFullPath(".").Replace('\\', '/') + "/";
            var path = EditorUtility.OpenFolderPanel("Select Directory", "Assets", "").Replace('\\', '/');
            var relative = new Regex(project).Replace(path, "");
            return relative;
        }

        public static T[] With<T>(this T[] array, T element) {
            var output = new T[array.Length + 1];
            for (var i = 0; i < array.Length; i++) {
                output[i] = array[i];
            }
            output[output.Length - 1] = element;
            return output;
        }

        public static T[] Without<T>(this T[] array, T element) where T : class {
            if (!array.Contains(element)) {
                return array;
            }
            var others = new List<T>(array.Length);
            foreach (var other in array) {
                if (other != element) {
                    others.Add(other);
                }
            }
            return others.ToArray();
        }

        public static float SqrDistance(Vector3 a, Vector3 b) {
            return new Vector3(b.x - a.x, b.y - a.y, b.z - a.z).sqrMagnitude;
        }

        public static GameObject[] FindPrefabsInProject(string baseDirectory, string[] ignoredDirectories = null) {
            var paths = AssetDatabase.FindAssets("t:Prefab", new[] { baseDirectory }).Select(AssetDatabase.GUIDToAssetPath);
            
            if (ignoredDirectories != null) {
                paths = paths.Where(path => !ignoredDirectories.Any(path.StartsWith));
            }
            
            return paths.Select(path => (GameObject) AssetDatabase.LoadAssetAtPath(path, typeof(GameObject))).ToArray();
        }
        
        public static GameObject[] FindUniquePrefabsInScene() {
            var unique = new HashSet<GameObject>();
            Ut.ApplyToAllGameObjects(obj => {
                var prefab = PrefabUtility.GetPrefabParent(obj) as GameObject;
                if (prefab != null) {
                    unique.Add(prefab);
                }
            });
            return unique.ToArray();
        }
        
        public static void ApplyToAllGameObjects(Action<GameObject> action) {
            var scene = UnityEngine.SceneManagement.SceneManager.GetActiveScene();
            foreach (var obj in scene.GetRootGameObjects()) {
                ApplyToAllGameObjectsInHierarchy(obj, action);
            }
        }
        
        public static void ApplyToAllGameObjectsInHierarchy(this GameObject obj, Action<GameObject> action) {
            var transform = obj.transform;
            var count = transform.childCount;
            
            action(obj);
            
            for (var i = 0; i < count; i++) {
                ApplyToAllGameObjectsInHierarchy(transform.GetChild(i).gameObject, action);
            }
        }

        public static void ApplyToAllGameObjectsInChildren(GameObject obj, Action<GameObject> action) {
            var transform = obj.transform;
            var count = transform.childCount;
            
            for (var i = 0; i < count; i++) {
                action(transform.GetChild(i).gameObject);
            }
        }

        public static GameObject[] FindGameObjectsWhere(Predicate<GameObject> predicate) {
            var found = new List<GameObject>();
            ApplyToAllGameObjects(obj => {
                if (predicate(obj)) {
                    found.Add(obj);
                }
            });
            return found.ToArray();
        }

        public static GameObject FindGameObjectWhere(Predicate<GameObject> predicate) {
            var scene = UnityEngine.SceneManagement.SceneManager.GetActiveScene();
            foreach (var obj in scene.GetRootGameObjects()) {
                var found = FindGameObjectInHierarchyWhere(obj, predicate);
                if (found != null) {
                    return found;
                }
            }
            return null;
        }

        static GameObject FindGameObjectInHierarchyWhere(GameObject obj, Predicate<GameObject> predicate) {
            if (predicate(obj)) {
                return obj;
            }
            var transform = obj.transform;
            var count = transform.childCount;
            
            for (var i = 0; i < count; i++) {
                var found = FindGameObjectInHierarchyWhere(transform.GetChild(i).gameObject, predicate);
                if (found != null) {
                    return found;
                }
            }
            return null;
        }
        
        public static bool GameObjectExistsWhere(Predicate<GameObject> predicate) {
            return FindGameObjectWhere(predicate) != null;
        }
        
        public static GameObject[] FindGameObjectsWithinDistance(Vector3 position, float distance) {
            return FindGameObjectsWhere(obj => SqrDistance(obj.transform.position, position) <= distance * distance);
        }

        public static Rect AsPositive(this Rect rect) {
            if (rect.width < 0) {
                rect.x = rect.x + rect.width;
                rect.width = -rect.width;
            }
            if (rect.height < 0) {
                rect.y = rect.y + rect.height;
                rect.height = -rect.height;
            }
            return rect;
        }

        public static void FocusSceneView() {
            if (SceneView.sceneViews.Count != 0) {
                ((SceneView) SceneView.sceneViews[0]).Focus();
            }
        }

        public static void SetEditorCursor(MouseCursor cursor) {
            EditorGUIUtility.AddCursorRect(new Rect(0f, 0f, 100000f, 100000f), cursor);
        }

        public static T[] InvokeAll<T>(this Delegate thing, params object[] arguments) {
            var functions = thing.GetInvocationList();
            var results = new T[functions.Length];
            for (var i = 0; i < functions.Length; i++) {
                results[i] = (T) functions[i].DynamicInvoke(arguments);
            }
            return results;
        }
    }
}
