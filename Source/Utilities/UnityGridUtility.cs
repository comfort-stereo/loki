using System;
using System.Reflection;

namespace LokiPlugin {
    public static class UnityGridUtility {
        static Type AnnotationUtility;
        static PropertyInfo CachedShowGridProperty;

        public static PropertyInfo ShowGridProperty {
            get {
                if (CachedShowGridProperty == null) {
                    AnnotationUtility = Type.GetType("UnityEditor.AnnotationUtility,UnityEditor.dll");
                    if (AnnotationUtility == null) {
                        return null;
                    }
                    CachedShowGridProperty = AnnotationUtility.GetProperty("showGrid", BindingFlags.Static | BindingFlags.NonPublic);
                }
                return CachedShowGridProperty;
            }
        }

        public static bool ShowUnityGrid {
            get { return (bool) UnityGridUtility.ShowGridProperty.GetValue(null, null); }
            set { UnityGridUtility.ShowGridProperty.SetValue(null, value, null); }
        }
    }
}