﻿using UnityEngine;

namespace LokiPlugin {
    
    public enum GridLockMode {
        Center,
        Corners
    }

    static class GridUtility {
        public static Vector3 MovePositionToGrid(Vector3 position, GridLockMode mode, float gridSize) {
            Vector2 adjusted;
            if (mode == GridLockMode.Center) {
                adjusted = new Vector2(
                    Mathf.Floor(position.x / gridSize) * gridSize + gridSize / 2,
                    Mathf.Floor(position.y / gridSize) * gridSize + gridSize / 2
                );
            } else {
                adjusted = new Vector2(
                    Mathf.Round(position.x / gridSize) * gridSize,
                    Mathf.Round(position.y / gridSize) * gridSize
                );
            }

            return adjusted;
        }
    }
}
