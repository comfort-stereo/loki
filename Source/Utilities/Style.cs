﻿using UnityEngine;
using UnityEditor;

namespace LokiPlugin {
    static class Style {
        public static readonly GUIStyle PaletteEntryLabelStyle = new GUIStyle().Apply(style => {
            style.alignment = TextAnchor.UpperCenter;
            style.fontSize = 9;
        });
        
        public static readonly GUIStyle Header = new GUIStyle(EditorStyles.largeLabel).Apply(style => {
            style.alignment = TextAnchor.MiddleCenter;
            style.fontSize = 10;
        });

        public static readonly GUIStyle LargeLabelCenter = new GUIStyle(EditorStyles.largeLabel).Apply(style => {
            style.alignment = TextAnchor.MiddleCenter;
            style.fontSize = 9;
        });
        
        public static readonly GUIStyle LabelCenter = new GUIStyle(EditorStyles.label).Apply(style => {
            style.alignment = TextAnchor.MiddleCenter;
        });
        
        public static readonly GUIStyle BoldLabelLeft = new GUIStyle(EditorStyles.label).Apply(style => {
            style.alignment = TextAnchor.MiddleLeft;
            style.fontStyle = FontStyle.Bold;
        });

        public static readonly GUIStyle Enclosed = new GUIStyle(EditorStyles.helpBox);
        public static readonly GUIStyle SelectionBox = new GUIStyle(EditorStyles.helpBox);
    }
}
