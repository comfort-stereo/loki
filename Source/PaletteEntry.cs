﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace LokiPlugin {
    class PaletteEntry {
        
        GameObject prefab;
        bool wasAddedDirectly;
        List<int> searchNumbers;

        public PaletteEntry(GameObject prefab, bool wasAddedDirectly = false, IEnumerable<int> searchNumbers = null) {
            this.prefab = prefab;
            this.wasAddedDirectly = wasAddedDirectly;
            this.searchNumbers = searchNumbers != null ? searchNumbers.ToList() : new List<int>();
        }

        public GameObject Prefab {
            get { return this.prefab; }
        }

        public bool WasAddedDirectly {
            get { return this.wasAddedDirectly; }
        }

        public int[] SearchNumbers {
            get { return this.searchNumbers.ToArray(); }
        }

        public void AddSearchNumber(int number) {
            this.searchNumbers.Add(number);
        }
    } 
}
