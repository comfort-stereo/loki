namespace LokiPlugin {
    class WindowController {
        
        public static readonly WindowController Blank = new WindowController();
        
        event QueryCallback onIsOpen;
        event QueryCallback onIsFocused;
        event Callback onOpen;
        event Callback onClose;
        event Callback onToggle;
        event Callback onFocus;
        event Callback onRefresh;
        
        public WindowController(
            QueryCallback onIsOpen = null, 
            QueryCallback onIsFocused = null,
            Callback onOpen = null, 
            Callback onClose = null, 
            Callback onToggle = null, 
            Callback onFocus = null, 
            Callback onRefresh = null
        ) {
            this.onIsOpen = onIsOpen;
            this.onIsFocused = onIsFocused;
            this.onOpen = onOpen;
            this.onClose = onClose;
            this.onToggle = onToggle;
            this.onFocus = onFocus;
            this.onRefresh = onRefresh;
        }
        
        public bool IsOpen {
            get {
                if (this.onIsOpen != null) {
                    return this.onIsOpen();
                }
                return false;
            }
        }

        public bool IsFocused {
            get {
                if (this.onIsFocused != null) {
                    return this.onIsFocused();
                }
                return false;
            }
        }

        public void Focus() {
            if (this.onFocus != null) {
                this.onFocus();
            }
        }

        public void Open() {
            if (this.onOpen != null) {
                this.onOpen();
            }
        }
        
        public void Close() {
            if (this.onClose != null) {
                this.onClose();
            }
        }
        
        public void Toggle() {
            if (this.onToggle != null) {
                this.onToggle();
            }
        }

        public void Refresh() {
            if (this.onRefresh != null) {
                this.onRefresh();
            }
        }
    }
}